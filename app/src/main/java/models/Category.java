package models;

/**
 * Created by win on 18/04/2018.
 */

public class Category
{
    private String producto="";
    private int cantidad;
    private double precioTotal;
    private double precioUnitario;
    private double entregado;
    private double saldo;
    private double transporte;
    public Category(String producto, int cantidad, double precioTotal, double precioUnitario, double entregado, double saldo, double transporte)
    {
        this.producto=producto;
        this.cantidad=cantidad;
        this.precioTotal=precioTotal;
        this.precioUnitario = precioUnitario;
        this.entregado = entregado;
        this.saldo = saldo;
        this.transporte = transporte;
    }

    public double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(double precioTotal) {
        this.precioTotal = precioTotal;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public double getEntregado() {
        return entregado;
    }
    public void setEntregado(double entregado) {
        this.entregado = entregado;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public double getTransporte() { return transporte; }

    public void setTransporte(double transporte) {
        this.transporte = transporte;
    }
}
