package models;

/**
 * Created by win on 05/01/2018.
 */

public class ClienteRazon {
    private int id;
    private String razonSocial;
    private long nit;


    public ClienteRazon(int id, String razonSocial, long nit){
        this.id=id;
        this.razonSocial=razonSocial;
        this.nit=nit;
    }

    public int getId(){
        return this.id;
    }

    public long getNit(){
        return this.nit;
    }

    @Override
    public String toString() {
        return razonSocial;
    }
}
