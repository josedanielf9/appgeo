package models;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by win on 05/01/2018.
 */

public class Cliente {
    private int id;
    private String razonSocial;
    private long nit;
    public double latitud;
    public double longitud;
    public double distancia;


    public Cliente(int id,String razonSocial,long nit){
        this.id=id;
        this.razonSocial=razonSocial;
        this.nit=nit;
    }

    public Cliente(int id,String razonSocial,long nit,double latitud,double longitud){
        this.id=id;
        this.razonSocial=razonSocial;
        this.nit=nit;
        this.latitud=latitud;
        this.longitud=longitud;
    }

    public Cliente(){

    }

    public int getId(){
        return this.id;
    }

    @Override
    public String toString() {
        return razonSocial;
    }
}
