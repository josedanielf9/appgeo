package models;

/**
 * Created by win on 05/01/2018.
 */

public class Destino {
    private int id;
    private String destino;
    private String direccion;


    public Destino(int id, String destino, String direccion){
        this.id=id;
        this.destino=destino;
        this.direccion=direccion;
    }

    public int getId(){
        return this.id;
    }
    public String getDireccion(){
        return this.direccion;
    }

    @Override
    public String toString() {
        return destino;
    }
}
