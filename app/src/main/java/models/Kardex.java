package models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by win on 05/01/2018.
 */

public class Kardex implements Serializable {

    public int idDestino;
    public int idCliente;
    public int idClienteRazon;
    public String nombreCliente;
    public String id_usuario;
    public String fecha;
    public String observaciones;
    public String servicio_transporte;
    public String valor;
    public String saldo;
    public ArrayList<DetalleKardex> detallesKardex;

    public Kardex(){
        detallesKardex =new ArrayList<>();
    }

    public Kardex(Kardex despacho){
        this.idDestino=despacho.idDestino;
        this.idCliente=despacho.idCliente;
        this.idClienteRazon=despacho.idClienteRazon;
        this.fecha=despacho.fecha;
        this.observaciones=despacho.observaciones;
        this.detallesKardex =despacho.detallesKardex;
        this.nombreCliente=despacho.nombreCliente;
        this.id_usuario=despacho.id_usuario;
        this.servicio_transporte = despacho.servicio_transporte;
        this.saldo = despacho.saldo;
        this.valor=despacho.valor;
    }

    public Kardex(int idDestino,int idCliente,int idClienteRazon,String fecha,String observaciones, String servicio_transporte){
        this.idDestino=idDestino;
        this.idCliente=idCliente;
        this.idClienteRazon=idClienteRazon;
        this.fecha=fecha;
        this.servicio_transporte=servicio_transporte;
        this.observaciones=observaciones;
        detallesKardex =new ArrayList<>();
    }


    public void setDetalleDespacho(ArrayList<DetalleKardex> detallesKardex){
        this.detallesKardex=detallesKardex;
    }

    @Override
    public String toString() {
        return fecha;
    }
}
