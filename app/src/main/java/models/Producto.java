package models;

/**
 * Created by win on 05/01/2018.
 */

public class Producto {
    private int id;
    private String nombre;
    private String imagen;
    public double cantidad;
    private double precio;


    public Producto(int id,String nombre,String imagen, double cantidad, double precio){
        this.id=id;
        this.nombre=nombre;
        this.cantidad=cantidad;
        this.precio=precio;
        this.imagen=imagen;
    }

    public int getId(){
        return this.id;
    }

    public String getNombre(){
        return nombre;
    }

    public String getImagen(){
        return imagen;
    }

    public Double getPrecio(){
        return precio;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
