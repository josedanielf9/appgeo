package models;

import java.io.Serializable;
import java.util.ArrayList;


/**
 * Created by win on 05/01/2018.
 */

public class VentaKardex implements Serializable {

    public int idEmpresa;
    public int idUsuario;
    public String fecha;
    public int idClienteRazon;
    public int idCliente;
    public ArrayList<DetalleDespacho> detallesDespacho;
    public ArrayList<Cliente> cliente;

    public VentaKardex(){
        detallesDespacho =new ArrayList<>();
    }

    public VentaKardex(VentaKardex despacho){
        this.idEmpresa=despacho.idEmpresa;
        this.idCliente=despacho.idCliente;
        this.idClienteRazon=despacho.idClienteRazon;
        this.fecha=despacho.fecha;
        this.detallesDespacho =despacho.detallesDespacho;
        this.idUsuario=despacho.idUsuario;
        this.cliente=despacho.cliente;
    }

    public VentaKardex(int idEmpresa,int idCliente,int idClienteRazon,String fecha,int idUsuario){
        this.idEmpresa=idEmpresa;
        this.idCliente=idCliente;
        this.idClienteRazon=idClienteRazon;
        this.fecha=fecha;
        this.idUsuario=idUsuario;
        detallesDespacho =new ArrayList<>();
        cliente = new ArrayList<>();
    }


    public void setDetalleDespacho(ArrayList<DetalleDespacho> detallesDespacho){
        this.detallesDespacho=detallesDespacho;
    }

    @Override
    public String toString() {
        return fecha;
    }
}
