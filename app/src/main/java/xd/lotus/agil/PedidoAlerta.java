package xd.lotus.agil;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import models.Cliente;
import models.ClienteRazon;
import models.Despacho;
import models.Destino;
import models.DetalleDespacho;
import utils.Functions;

public class PedidoAlerta extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    JSONObject jsonObject;
    JSONArray jsonArray;
    JSONObject jsonArrayAlerta;
    ArrayList<Cliente> clientes;
    ArrayList<ClienteRazon> clientesRazon;
    ArrayList<Destino> destinos;
    Despacho despacho;
    String id_empresa,id_usuario;

    private Location mylocation;
    private GoogleApiClient googleApiClient;
    private final static int REQUEST_CHECK_SETTINGS_GPS=0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS=0x2;

    short geof =0;
    private int mYear, mMonth, mDay;
    EditText fecha;
    ArrayList<String>facturar;
    ArrayList<Integer>nit;
    ArrayList<String>destino;
    ArrayList<String>direccion;
    Spinner spinnerFactura;
    ArrayAdapter<String>facturaAdapter;
    Spinner spinnerDestino;
    ArrayAdapter<String>destinoAdapter;
    TextView textDireccion;
    ArrayAdapter<String>direccionAdapter;
    TextView nitView;
    AutoCompleteTextView nombreCLiente;
    TextView obs;
    String guardadoLocal="";
    String guardadoFactura="";
    int numero;
    String limite="";
    Button addFacturar;
    Button addDestino;
    LinearLayout facturarPedido;
    String alertArray="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedido);
        getSupportActionBar().setTitle("PEDIDO");

        id_empresa= getIntent().getStringExtra("ID_EMPRESA");
        id_usuario= getIntent().getStringExtra("ID_USUARIO");
        guardadoLocal= getIntent().getStringExtra("GUARDADO_LOCAL");
        guardadoFactura= getIntent().getStringExtra("GUARDADO_FACTURA");
        numero= getIntent().getIntExtra("NUMEROX",0);
        alertArray = getIntent().getStringExtra("ALERT");

        try {
            jsonArrayAlerta = new JSONObject(alertArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        clientes=new ArrayList<>();

        clientesRazon=new ArrayList<>();
        destinos=new ArrayList<>();

        limite = getIntent().getStringExtra("ACTIVITY");
//        new cargarProductos().execute(Functions.REST_URL +"gtm-detalle-despacho-alerta/empresa/"+id_empresa+"/inicio/0/fin/0/empleado/0/cliente/0/usuario/"+id_usuario);

        final Button geo = findViewById(R.id.habilitarGeo);
        geo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(geof==0) {
                    geo.setBackgroundResource(R.drawable.nogeo);geof=1;
                }
                else{
                    geo.setBackgroundResource(R.drawable.geo); geof=0;
                    nombreCLiente.setError(null);
                    //calcular distancia menor
                    new LecturaTodosClientesEmpresa().execute(Functions.REST_URL +"clientes/empresa/"+id_empresa);
                }
            }
        });

        setUpGClient();

        fecha = findViewById(R.id.fecha);
        obs = findViewById(R.id.obs);

        obtenerFecha();

        Button fechaC = findViewById(R.id.fechaC);
        fechaC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker();
            }
        });

        facturar= new ArrayList<>();
        nit=new ArrayList<>();
        nitView = findViewById(R.id.nitView);
        destino = new ArrayList<>();
        direccion = new ArrayList<>();

        obs.setText(getIntent().getStringExtra("OBS_K"));

        addDestino = findViewById(R.id.addDestino);
        addDestino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDestinos();
            }
        });

        addDestino.setEnabled(false);

        Button ok = findViewById(R.id.okb);
        Button nook = findViewById(R.id.nook);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pasarInventario();
            }
        });

        nook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(PedidoAlerta.this, "Cancelado", Toast.LENGTH_SHORT).show();
                Intent i=new Intent(PedidoAlerta.this, Select.class);
                i.putExtra("ID_EMPRESA",id_empresa);
                i.putExtra("ID_USUARIO",id_usuario);
                startActivity(i);
            }
        });

        addFacturar = findViewById(R.id.addFacturar);
        addFacturar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPersona();
            }
        });
        addFacturar.setEnabled(false);

        nombreCLiente = (AutoCompleteTextView)findViewById(R.id.nombreCliente);
        spinnerDestino = findViewById(R.id.spinnerDestino);
        textDireccion = findViewById(R.id.textDireccion);
        spinnerFactura = findViewById(R.id.spinnerFactura);
        facturarPedido = findViewById(R.id.facturarPedido);

//                habilitar campos transportes y precio =========
        try {
//            Log.e("el dato de alerta", jsonArrayAlerta.getJSONObject(numero).toString());
            if (jsonArrayAlerta.getJSONObject("detalle_Kardex") != null) {
                facturarPedido.setVisibility(View.GONE);
                nitView.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(guardadoLocal.equals("1")){
//          ==== agregando los  datos en el formulario de pedido ======
            despacho=(models.Despacho)getIntent().getSerializableExtra("despacho");

            nombreCLiente.setText(despacho.nombreCliente);
            nombreCLiente.setEnabled(false);
            geo.setEnabled(false);

            try {
                Log.d("recibido", jsonArrayAlerta.toString());
                obs.setText(jsonArrayAlerta.getJSONObject("despacho").getString("observacion"));
                establecerCliente(null,0,despacho.idCliente);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            despacho=new Despacho();
        }

        nombreCLiente.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View arg0, int arg1, KeyEvent arg2) {
                // TODO Auto-generated method stub
                if ((arg2.getAction() == KeyEvent.ACTION_DOWN) &&
                        (arg1 == KeyEvent.KEYCODE_ENTER)) {
                    String text=nombreCLiente.getText().toString().replace(" ","%20");

                    new LecturaClientesEmpresa().execute(Functions.REST_URL +"clientes/empresa/"+id_empresa+"/texto/"+text);
                }

                // return true; - if consumed
                return false;
            }
        });

        nombreCLiente.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                establecerCliente(parent,position,0);
            }
        });


        spinnerFactura.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ClienteRazon item = (ClienteRazon)parent.getItemAtPosition(position);
                nitView.setText(item.getNit()+"");
                despacho.idClienteRazon=item.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinnerDestino.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Destino item = (Destino)parent.getItemAtPosition(position);
                textDireccion.setText(item.getDireccion()+"");
                despacho.idDestino=item.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    //private method of your class
    private int getIndex(Spinner spinner, String myString){
        for (int i=0;i<spinner.getCount();i++){

            Log.d("posicion", spinner.getItemAtPosition(i).toString());
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){

                return i;
            }
        }
        Log.d("posicion", String.valueOf(spinner.getItemAtPosition(1)));
        return 0;
    }

    private synchronized void setUpGClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        mylocation = location;
        if (mylocation != null) {
            Double latitude=mylocation.getLatitude();
            Double longitude=mylocation.getLongitude();

        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        checkPermissions();
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Do whatever you need
        //You can display a message here
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //You can display a message here
    }

    private void checkPermissions(){
        int permissionLocation = ContextCompat.checkSelfPermission(PedidoAlerta.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this,
                        listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            }
        }else{
            getMyLocation();
        }

    }

    private void getMyLocation(){
        if(googleApiClient!=null) {
            if (googleApiClient.isConnected()) {
                int permissionLocation = ContextCompat.checkSelfPermission(PedidoAlerta.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    mylocation =                     LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(3000);
                    locationRequest.setFastestInterval(3000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi
                            .requestLocationUpdates(googleApiClient, locationRequest, this);
                    PendingResult<LocationSettingsResult> result =
                            LocationServices.SettingsApi
                                    .checkLocationSettings(googleApiClient, builder.build());
                    result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                        @Override
                        public void onResult(LocationSettingsResult result) {
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.
                                    int permissionLocation = ContextCompat
                                            .checkSelfPermission(PedidoAlerta.this,
                                                    Manifest.permission.ACCESS_FINE_LOCATION);
                                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                                        mylocation = LocationServices.FusedLocationApi
                                                .getLastLocation(googleApiClient);
                                    }
                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.
                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        status.startResolutionForResult(PedidoAlerta.this,
                                                REQUEST_CHECK_SETTINGS_GPS);
                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied.
                                    // However, we have no way
                                    // to fix the
                                    // settings so we won't show the dialog.
                                    // finish();
                                    break;
                            }
                        }
                    });
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        int permissionLocation = ContextCompat.checkSelfPermission(PedidoAlerta.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            getMyLocation();
        }
    }

    public void establecerCliente(AdapterView<?> parent,int position,int idCliente){
        if(idCliente==0){
            Cliente item = (Cliente)parent.getItemAtPosition(position);
            despacho.idCliente=item.getId();
            new LecturaCliente().execute(Functions.REST_URL +"clientes/"+item.getId());
        }else{
            despacho.idCliente=idCliente;
            new LecturaCliente().execute(Functions.REST_URL +"clientes/"+idCliente);
        }
        addFacturar.setEnabled(true);
        addDestino.setEnabled(true);
    }

    private class LecturaTodosClientesEmpresa extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            return Functions.readJSONFeed(urls[0]);
        }

        protected void onPostExecute(String result) {
            try {
                // Locate the NodeList name
                jsonArray = new JSONArray(result);
                clientes=new ArrayList<>();
                Cliente clienteAproximado=new Cliente();
                clienteAproximado.distancia=9999999;
                for (int i = 0; i < jsonArray.length(); i++) {
                    jsonObject = jsonArray.getJSONObject(i);
                    Cliente cliente = new Cliente(jsonObject.getInt("id"),jsonObject.getString("razon_social"),jsonObject.getLong("nit"),jsonObject.getDouble("latitud"),jsonObject.getDouble("longitud"));
                    clientes.add(cliente);
                    Location locationCliente = new Location("");
                    locationCliente.setLatitude(cliente.latitud);
                    locationCliente.setLongitude(cliente.longitud);
                    double actualLatitud=mylocation.getLatitude();
                    double actualLongitud=mylocation.getLongitude();
                    double distancia=locationCliente.distanceTo(mylocation);
                    clienteAproximado=obtenerMenorDistancia(clienteAproximado.distancia,clienteAproximado,distancia,cliente);
                }
                establecerCliente(null,0,clienteAproximado.getId());
                nombreCLiente.setText(clienteAproximado.toString());

            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
        }

        public Cliente obtenerMenorDistancia(double distancia1,Cliente c1,double distancia2,Cliente c2){
            Cliente res;
            if(distancia1<distancia2){
                res=c1;
                res.distancia=distancia1;
            }else{
                res=c2;
                res.distancia=distancia2;
            }
            return res;
        }
    }

    private class LecturaClientesEmpresa extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            return Functions.readJSONFeed(urls[0]);
        }

        protected void onPostExecute(String result) {
            try {
                Log.e("el datoooooooooo", result);
                jsonArray = new JSONArray(result);
                if (jsonArray.length()>0) {
                    // Locate the NodeList name

                    clientes = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);
                        Cliente cliente = new Cliente(jsonObject.getInt("id"), jsonObject.getString("razon_social"), jsonObject.getLong("nit"));
                        clientes.add(cliente);

                        ArrayAdapter<Cliente> adapter =
                                new ArrayAdapter<Cliente>(PedidoAlerta.this, android.R.layout.simple_list_item_1, clientes);
                        nombreCLiente.setAdapter(adapter);
                        nombreCLiente.showDropDown();

                    }
                }else{

                    Log.e("el dato escrito ", String.valueOf(nombreCLiente.getText()));
                    addCliente(String.valueOf(nombreCLiente.getText()));


                }
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
        }
    }


    public void addCliente(String nombre)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater layoutInflater = this.getLayoutInflater();
        final View dialog = layoutInflater.inflate(R.layout.cliente, null);
        builder.setView(dialog);
        final AlertDialog alertDialog = builder.create();

        //cosas
        final EditText addNombres = dialog.findViewById(R.id.nombreClienteR);
        addNombres.setText(nombre);
        final EditText addTelefono = dialog.findViewById(R.id.telefonoCliente);
        final EditText addContacto = dialog.findViewById(R.id.contactoCliente);
        Button cancelarAdd = dialog.findViewById(R.id.cancelarAdd);
        Button aceptarAdd = dialog.findViewById(R.id.aceptarAdd);

        cancelarAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        aceptarAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(addNombres.getText().toString().length() == 0)
                {
                    addNombres.setError("ingrese el nombre");
                    addNombres.requestFocus();
                }
                else {
                    new GuardarClienteNuevo().execute(addNombres.getText().toString(), addTelefono.getText().toString(), addContacto.getText().toString());
                    nombreCLiente.setText(addNombres.getText().toString());
                    addFacturar.setEnabled(true);
                    addDestino.setEnabled(true);
                    Toast.makeText(PedidoAlerta.this, "El cliente se registro", Toast.LENGTH_SHORT).show();
                    alertDialog.dismiss();
                }
            }
        });
        alertDialog.show();
    }

    private class GuardarClienteNuevo extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... data) {
            JSONObject datos = new JSONObject();
            try {
                datos.put("razon_social",data[0]);
                datos.put("telefono",data[1]);
                datos.put("contacto",data[2]);

                JSONObject res = Functions.postJSONdata("clientes-pedido/"+id_empresa,datos);

//                String idCliente = String.valueOf(resultado.getJSONObject("id"));
                Log.e("el resultadooo", String.valueOf(res));
                String idCliente = res.getString("id");
                Log.e("el ideeeeeeeeee", String.valueOf(idCliente));
                despacho.idCliente=Integer.valueOf(idCliente);


                establecerCliente(null,0,Integer.valueOf(idCliente));
            } catch (JSONException ex) {
                //Logger.getLogger(EditPeymentConcept.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "";
        }
    }

    private class LecturaCliente extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            return Functions.readJSONFeed(urls[0]);
        }

        protected void onPostExecute(String result) {
            try {
                // Locate the NodeList name
                jsonObject = new JSONObject(result);
                JSONArray clientesDestinoJson = jsonObject.getJSONArray("cliente_destinos");
                destinos=new ArrayList<>();
                Integer posDestino = 0;
                if (clientesDestinoJson.length()>0) {
                    for (int i = 0; i < clientesDestinoJson.length(); i++) {
                        JSONObject clienteDestinoJson = clientesDestinoJson.getJSONObject(i);
                        JSONObject destinoJson = clienteDestinoJson.getJSONObject("destino");
//                        ======== para seleccion de destino ========
                        if (jsonArrayAlerta.getJSONObject("despacho").getInt("id_destino") == destinoJson.getInt("id")){
                            posDestino = i;
                        }
                        Destino destino = new Destino(destinoJson.getInt("id"), destinoJson.getString("destino"), destinoJson.getString("direccion"));

                        destinos.add(destino);


                        MySpinnerAdapter adapter =
                                new MySpinnerAdapter(PedidoAlerta.this,
                                        android.R.layout.simple_spinner_item,
                                        destinos);

                        spinnerDestino.setAdapter(adapter);
                        spinnerDestino.setSelection(posDestino);

                    }
                }

                clientesRazon=new ArrayList<>();
                if (jsonObject.getJSONArray("clientes_razon").length() > 0) {
                    Integer posClienteRazon = 0;
                    for (int i = 0; i < jsonObject.getJSONArray("clientes_razon").length(); i++) {
                        JSONObject clienteRazonJson = jsonObject.getJSONArray("clientes_razon").getJSONObject(i);
                         //======== para seleccion de destino ========
                        if (jsonArrayAlerta.getJSONObject("despacho").getInt("id_cliente_razon") == clienteRazonJson.getInt("id")){
                            posClienteRazon = i;
                        }

                        ClienteRazon clienteRazon = new ClienteRazon(clienteRazonJson.getInt("id"), clienteRazonJson.getString("razon_social"), clienteRazonJson.getLong("nit"));

                        clientesRazon.add(clienteRazon);

                        ArrayAdapter<ClienteRazon> adapter = new ArrayAdapter<ClienteRazon>(PedidoAlerta.this,
                                android.R.layout.simple_spinner_item,
                                clientesRazon);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinnerFactura.setAdapter(adapter);
                        spinnerFactura.setSelection(posClienteRazon);

                    }
                }
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
        }
    }


    public void obtenerFecha()
    {

        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 1);
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd/MM/yyyy");

        fecha.setText(simpleDateFormat1.format(c.getTime()));
//        fecha.setText(Integer.toString(mDay)+"/"+Integer.toString(mMonth)+"/"+mYear);
    }

    public void darFecha(int x, int y, int z)
    {
        mYear = x;
        mMonth = y;
        mDay = z;
        fecha.setText(Integer.toString(mDay)+"/"+Integer.toString(mMonth)+"/"+mYear);
    }

    public void picker()
    {
        final DatePickerDialog dl = new DatePickerDialog(this, null, mYear,mMonth-1,mDay);
        dl.setButton(DialogInterface.BUTTON_POSITIVE, "Guardar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                darFecha(dl.getDatePicker().getYear(),dl.getDatePicker().getMonth()+1,dl.getDatePicker().getDayOfMonth());
                dl.dismiss();
            }
        });
        dl.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dl.dismiss();
            }
        });
        dl.show();
    }

    public void addPersona()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater layoutInflater = this.getLayoutInflater();
        final View dialog = layoutInflater.inflate(R.layout.fechapicker, null);
        builder.setView(dialog);
        final AlertDialog alertDialog = builder.create();

        //cosas
        final EditText addNombres = dialog.findViewById(R.id.addNombres);
        final EditText addNits = dialog.findViewById(R.id.addNits);
        Button cancelarAdd = dialog.findViewById(R.id.cancelarAdd);
        Button aceptarAdd = dialog.findViewById(R.id.aceptarAdd);

        cancelarAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        aceptarAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(addNombres.getText().toString().length() == 0)
                {
                    addNombres.setError("ingrese el nombre");
                    addNombres.requestFocus();
                }
                else if(addNits.getText().toString().length()== 0)
                {
                    addNits.setError("ingrese el nit");
                    addNits.requestFocus();
                }
                else {
                    new GuardarClienteRazon().execute(addNombres.getText().toString(), addNits.getText().toString());
                    alertDialog.dismiss();
                }
            }
        });
        alertDialog.show();
    }
    public void addDestinos()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater layoutInflater = this.getLayoutInflater();
        final View dialog = layoutInflater.inflate(R.layout.destinopicker, null);
        builder.setView(dialog);
        final AlertDialog alertDialog = builder.create();

        //cosas
        final EditText addDestinos = dialog.findViewById(R.id.addDestinoX);
        final EditText addDireccion = dialog.findViewById(R.id.addDireccionXf);
        Button cancelarDestino = dialog.findViewById(R.id.cancelarDestino);
        Button aceptarDestino = dialog.findViewById(R.id.aceptarDestino);


        cancelarDestino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        aceptarDestino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(addDestinos.getText().toString().length() == 0)
                {
                    addDestinos.setError("ingrese el destino");
                    addDestinos.requestFocus();
                }
                else if(addDireccion.getText().toString().length()== 0)
                {
                    addDireccion.setError("ingrese la direccion");
                    addDireccion.requestFocus();
                }
                else {
                    new GuardarClienteDestino().execute(addDestinos.getText().toString(), addDireccion.getText().toString());
                    Log.e("llego", "llego a elseeee");
                    alertDialog.dismiss();
                }
            }
        });
        alertDialog.show();

    }


    public void pasarInventario()
    {

        despacho.fecha=fecha.getText().toString();
        TextView errorTextFactura = (TextView)spinnerFactura.getSelectedView();
        TextView errorTextDestino = (TextView)spinnerDestino.getSelectedView();
//        View selectedView = spinnerFactura.getSelectedView();
//        TextView selectedTextView = (TextView) selectedView;

        if(nombreCLiente.getText().toString().length() == 0)
        {
            nombreCLiente.setError("ingrese el cliente");
            nombreCLiente.requestFocus();
        }

        else if(guardadoFactura.equals("0") && errorTextFactura == null){
            TextView tvInvisibleError = findViewById(R.id.tvFacturaError);
            tvInvisibleError.requestFocus();
            tvInvisibleError.setError("Agregue una factura");

        }


        else if(errorTextDestino == null){
            TextView tvInvisibleErrorD = findViewById(R.id.tvDestinoError);
            tvInvisibleErrorD.requestFocus();
            tvInvisibleErrorD.setError("Agregue un destino");
        }

        else if(guardadoFactura.equals("0") && nitView.getText().toString().length() == 0){
            nitView.setError("ingrese el nit");
            nitView.requestFocus();
        }

        else if(textDireccion.getText().toString().length() == 0){
            textDireccion.setError("ingrese la direccion");
            textDireccion.requestFocus();
        }

        else {

            if (guardadoLocal.equals("1")) {
                despacho.observaciones = obs.getText().toString();
                guardarTodo();
            } else {
//                para enviar a detalle inventario cuando es pedido normal sin kardex
                despacho.observaciones = obs.getText().toString();
                Intent i = new Intent(PedidoAlerta.this, Inventario.class);
                i.putExtra("KEY_CLIENTE", nombreCLiente.getText().toString());
                i.putExtra("KEY_DESPACHO", fecha.getText().toString());
                i.putExtra("KEY_FACTURAR", spinnerFactura.getSelectedItem().toString());
                i.putExtra("KEY_NIT", nitView.getText().toString());
                i.putExtra("KEY_DESTINO", spinnerDestino.getSelectedItem().toString());
                i.putExtra("KEY_DIRECCION", textDireccion.getText().toString());
                i.putExtra("KEY_OBS", obs.getText().toString());
                i.putExtra("ACTIVITY", limite);
                i.putExtra("despacho", despacho);
                i.putExtra("ID_EMPRESA", id_empresa);
                i.putExtra("ID_USUARIO", id_usuario);
                startActivity(i);
            }
        }
    }

    public void guardarTodo()
    {
        Log.d("los datos de alertA ", jsonArrayAlerta.toString());
        JSONObject datosDespacho = new JSONObject();
        try {
            double cantidad = Double.parseDouble(despacho.detallesDespacho.get(0).cantidad);
            double precio_unitario = Double.parseDouble(despacho.detallesDespacho.get(0).precioUnitario);
            datosDespacho.put("id_detalle_despacho", jsonArrayAlerta.getString("id"));
            datosDespacho.put("cantidad", despacho.detallesDespacho.get(0).cantidad);
            datosDespacho.put("precio_unitario", despacho.detallesDespacho.get(0).precioUnitario);
            datosDespacho.put("servicio_transporte", despacho.detallesDespacho.get(0).transporte);
            datosDespacho.put("destino", despacho.idDestino);
            datosDespacho.put("cliente_razon", despacho.idClienteRazon);
            datosDespacho.put("observacion", despacho.observaciones);
            datosDespacho.put("total", cantidad * precio_unitario);
            datosDespacho.put("id_despacho", jsonArrayAlerta.getJSONObject("despacho").getInt("id"));
            ActualizarDespachoAlerta guardar = new ActualizarDespachoAlerta();
            guardar.datos = datosDespacho;
            guardar.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        Toast.makeText(PedidoAlerta.this, "Pedido exitoso", Toast.LENGTH_SHORT).show();
        Intent i=new Intent(PedidoAlerta.this, Alertar.class);
        i.putExtra("ID_EMPRESA",id_empresa);
        i.putExtra("ID_USUARIO",id_usuario);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    private class ActualizarDespachoAlerta extends AsyncTask<Object, Void, Object> {
        protected JSONObject datos;

        public ActualizarDespachoAlerta(){
            this.datos = datos;
        }

        @Override
        protected String doInBackground(Object... objects) {

            Functions.putJSONdata("gtm-detalle-despacho-alerta-actualizar", datos);

            return "";
        }
    }


    private class GuardarClienteRazon extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... data) {
            JSONObject datos = new JSONObject();
            try {
                datos.put("razon_social",data[0]);
                datos.put("nit",data[1]);
                Functions.postJSONdata("clientes-razon/"+despacho.idCliente,datos);
                establecerCliente(null,0,despacho.idCliente);
            } catch (JSONException ex) {
                //Logger.getLogger(EditPeymentConcept.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "";
        }
    }

    private static final String TAG = "CreadorDePDF";

    private class GuardarClienteDestino extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... data) {
            JSONObject datos = new JSONObject();
            try {


                datos.put("id_empresa",id_empresa);
                datos.put("destino",data[0]);
                datos.put("direccion",data[1]);

                Functions.postJSONdata("clientes-destino/"+despacho.idCliente,datos);
                establecerCliente(null,0,despacho.idCliente);
            } catch (JSONException ex) {
                //Logger.getLogger(EditPeymentConcept.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "";
        }
    }


}
