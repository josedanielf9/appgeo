package xd.lotus.agil;

import android.media.Image;

public class Movie {
    private String nom;
    private int img;


    public Movie() {
    }

    public Movie(String nom, int img) {
        super();
        this.nom = nom;
        this.img = img;
    }

    public String getNom()
    {
        return nom;
    }
    public int getImg()
    {
        return img;
    }
    public void setNom(String nom)
    {
        this.nom = nom;
    }
    public void  setImg(int img)
    {
        this.img=img;
    }
}
