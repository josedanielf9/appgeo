package xd.lotus.agil;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.pdf.PdfDocument;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextPaint;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import javax.crypto.spec.DESedeKeySpec;

import models.Category;
import models.Cliente;
import models.ClienteRazon;
import models.Despacho;
import utils.Functions;

public class PedidoKardexFactura extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    short geof =0;
    //    ArrayList<Despacho>pendientes;
    ArrayList<HashMap<String, String>> pendientes;
    SharedPreferences preferences;
    AutoCompleteTextView nombreCliente;
    EditText observaciones;
    ListView lv;
    JSONObject jsonObject;
    JSONArray jsonArray;
    ArrayList<Cliente> clientes;
    Despacho despacho;
    Despacho desp;

    Context mContext;

    ArrayList<Despacho>totK;
    ArrayList<ClienteRazon> clientesRazon;
    Button addFacturar;

    private File pdfFile;
    final private int REQUEST_CODE_ASK_PERMISSIONS = 111;
    private static final String TAG = "CreadorDePDF";

    AdapterItemKardex adapter;
    String id_empresa,id_usuario;

    private ProgressBar bar;

    private Location mylocation;
    private GoogleApiClient googleApiClient;
    private final static int REQUEST_CHECK_SETTINGS_GPS=0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS=0x2;
    Spinner spinnerFactura;
    ArrayList<Integer>nit;
    TextView nitView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedido_kardex_factura);
        getSupportActionBar().setTitle("Venta en Kardex Facturado");
        pendientes = new ArrayList<>();
        id_empresa= getIntent().getStringExtra("ID_EMPRESA");
        id_usuario= getIntent().getStringExtra("ID_USUARIO");
        despacho=new Despacho();

        bar = (ProgressBar) this.findViewById(R.id.progressBar);
        spinnerFactura = findViewById(R.id.spinnerFactura);
        nit=new ArrayList<>();
        nitView = findViewById(R.id.nitView);

        spinnerFactura.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ClienteRazon item = (ClienteRazon)parent.getItemAtPosition(position);
                nitView.setText(item.getNit()+"");
                despacho.idClienteRazon=item.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        addFacturar = findViewById(R.id.addFacturar);
        addFacturar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ingresarFacturar();
                addPersona();
            }
        });
        addFacturar.setEnabled(false);




        final Button geo = findViewById(R.id.habilitarGeoK);
        geo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(geof==0) {
                    geo.setBackgroundResource(R.drawable.nogeo);geof=1;
                }
                else{
                    geo.setBackgroundResource(R.drawable.geo); geof=0;
                    //calcular distancia menor
                    new LecturaTodosClientesEmpresa().execute(Functions.REST_URL +"clientes/empresa/"+id_empresa);

                    //Toast.makeText(Pedido.this,"latitude "+ mylocation.getLatitude(), Toast.LENGTH_SHORT).show();
                    //Toast.makeText(Pedido.this,"longitude "+ mylocation.getLongitude(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        setUpGClient();

//        int permissionCheck = ContextCompat.checkSelfPermission(PedidoKardex.this,
//                Manifest.permission.WRITE_EXTERNAL_STORAGE);
//        if(permissionCheck != PackageManager.PERMISSION_GRANTED) {
//            // ask permissions here using below code
//            ActivityCompat.requestPermissions(PedidoKardex.this,
//                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                    10);
//        }

        nombreCliente = (AutoCompleteTextView)findViewById(R.id.nombreClienteK);


        nombreCliente.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View arg0, int arg1, KeyEvent arg2) {
                // TODO Auto-generated method stub
                if ((arg2.getAction() == KeyEvent.ACTION_DOWN) &&
                        (arg1 == KeyEvent.KEYCODE_ENTER)) {
                    String text=nombreCliente.getText().toString().replace(" ","%20");

                    new LecturaClientesEmpresa().execute(Functions.REST_URL +"clientes/empresa/"+id_empresa+"/texto/"+text);
                }
                // return true; - if consumed
                return false;
            }
        });

        nombreCliente.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                establecerCliente(parent,position,0);
            }
        });
        observaciones = findViewById(R.id.obsK);


        //totK = new ArrayList<>();

        lv = findViewById(R.id.listaKardex);
//        adapter = new AdapterItemKardex(this, pendientes);
        //adapter = new AdapterItemKardex(this, totK);
//        cargarKardex();
        new GetKardex().execute(Functions.REST_URL +"gtm-despacho-kardex-factura/empresa/"+id_empresa+"/usuario/"+id_usuario+"/factura/"+true);
        //buscar();


//        lv.setAdapter(adapter);
        //cargarKardex();
        //Toast.makeText(this, pendientes.get(1).nombreCliente, Toast.LENGTH_SHORT);




        Button aceptarK = findViewById(R.id.aceptarK);
//        Button cancelarK = findViewById(R.id.cancelarK);


        aceptarK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView errorTextFactura = (TextView)spinnerFactura.getSelectedView();
                if(nombreCliente.getText().toString().length() == 0)
                {
                    nombreCliente.setError("ingrese el cliente");
                    nombreCliente.requestFocus();
                }
                else if(errorTextFactura == null){
                    TextView tvInvisibleError = findViewById(R.id.tvFacturaError);
                    tvInvisibleError.requestFocus();
                    tvInvisibleError.setError("Agregue una factura");
                }
                else if(nitView.getText().toString().length() == 0) {
                    nitView.setError("ingrese el nit");
                    nitView.requestFocus();
                }else{
                    despacho.observaciones=observaciones.getText().toString();
                    despacho.nombreCliente=nombreCliente.getText().toString();
                    Intent i = new Intent(PedidoKardexFactura.this, ProductosKardexFactura.class);
                    i.putExtra("despacho", despacho);
                    i.putExtra("GUARDADO_LOCAL", "1");
                    i.putExtra("NOMBRE_KARDEX",nombreCliente.getText().toString());
                    i.putExtra("OBS_KARDEX", observaciones.getText().toString());
                    i.putExtra("ID_EMPRESA",id_empresa);
                    i.putExtra("ID_USUARIO",id_usuario);
                    startActivity(i);
                }


            }
        });

        Button cancelar = findViewById(R.id.cancelar);
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(PedidoKardexFactura.this, "Cancelado", Toast.LENGTH_SHORT).show();
                Intent i=new Intent(PedidoKardexFactura.this, Select.class);
                i.putExtra("ID_EMPRESA",id_empresa);
                i.putExtra("ID_USUARIO",id_usuario);
                startActivity(i);
            }
        });
//        cancelarK.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(PedidoKardex.this, "Cancelado", Toast.LENGTH_SHORT).show();
//                Intent i=new Intent(PedidoKardex.this, Select.class);
//                i.putExtra("ID_EMPRESA",id_empresa);
//                i.putExtra("ID_USUARIO",id_usuario);
//                startActivity(i);
//            }
//        });
    }

    public void addPersona()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater layoutInflater = this.getLayoutInflater();
        final View dialog = layoutInflater.inflate(R.layout.fechapicker, null);
        builder.setView(dialog);
        final AlertDialog alertDialog = builder.create();

        //cosas
        final EditText addNombres = dialog.findViewById(R.id.addNombres);
        final EditText addNits = dialog.findViewById(R.id.addNits);
        Button cancelarAdd = dialog.findViewById(R.id.cancelarAdd);
        Button aceptarAdd = dialog.findViewById(R.id.aceptarAdd);

        cancelarAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        aceptarAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(addNombres.getText().toString().length() == 0)
                {
                    addNombres.setError("ingrese el nombre");
                    addNombres.requestFocus();
                }
                else if(addNits.getText().toString().length()== 0)
                {
                    addNits.setError("ingrese el nit");
                    addNits.requestFocus();
                }
                else {
                    new GuardarClienteRazon().execute(addNombres.getText().toString(), addNits.getText().toString());
                    alertDialog.dismiss();
                }
            }
        });
        alertDialog.show();
    }

    private class GuardarClienteRazon extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... data) {
            JSONObject datos = new JSONObject();
            try {
                datos.put("razon_social",data[0]);
                datos.put("nit",data[1]);
                Functions.postJSONdata("clientes-razon/"+despacho.idCliente,datos);
                establecerCliente(null,0,despacho.idCliente);
            } catch (JSONException ex) {
                //Logger.getLogger(EditPeymentConcept.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "";
        }
    }


    private class GetKardex extends AsyncTask<String, Void, String> {

        protected String doInBackground(String... urls) {
            return Functions.readJSONFeed(urls[0]);
        }

        @Override
        protected void onPreExecute(){
            bar.setVisibility(View.VISIBLE);
        }

        protected void onPostExecute(String result) {
            bar.setVisibility(View.GONE);
            try {
                // Locate the NodeList name
                ArrayList<Despacho> callLog = new ArrayList<Despacho>();
                Gson gson = new Gson();
//                descomponer el resultado y mandarlo al array modelo de pendientes =======================
//                =========================================================================================

                jsonArray = new JSONArray(result);
                pendientes=new ArrayList<>();

                for (int i = 0; i < jsonArray.length(); i++) {
                    Log.d("dato22: ", String.valueOf(i));
                    jsonObject = jsonArray.getJSONObject(i);


                    String id = jsonObject.getString("id");
                    JSONObject cliente = jsonObject.getJSONObject("cliente");
                    String nombre = cliente.getString("razon_social");

                    String fecha = jsonObject.getString("fecha");
                    String id_usuario = jsonObject.getString("id_usuario");
                    // tmp hash map for single contact
                    HashMap<String, String> contact = new HashMap<>();
                    id_usuario = getIntent().getStringExtra("ID_USUARIO");
                    String observacion = jsonObject.getString("observacion");


                    contact.put("id", id);
                    contact.put("nombreCliente", nombre);
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
                    Date date = dateFormat.parse(fecha);
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

                    contact.put("fecha", formatter.format(date));
                    contact.put("observacion", observacion);

                    pendientes.add(contact);
                    ListAdapter adapter = new SimpleAdapter(
                            PedidoKardexFactura.this, pendientes,
                            R.layout.item_kardex, new String[]{"nombreCliente", "fecha"}, new int[]{R.id.nombreKardex,
                            R.id.codigoKardex});

                    lv.setAdapter(adapter);
                }



//                if (result.isEmpty()) {
//                    callLog = new ArrayList<Despacho>();
//                    Log.e("llegooo ", "sin reultttttttt");
//                } else {
//                    Log.e("llegooo ", "con reultttttttt");
//                    Type type = new TypeToken<ArrayList<Despacho>>() {
//                    }.getType();
//                    Log.e("los datos base", String.valueOf(result));
//                    callLog = gson.fromJson(result, type);
//                }
//
//
//
//                pendientes.clear();
//                pendientes.addAll(callLog);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
        }

    }

//    public void buscar()
//    {
//        ArrayList<Despacho>mol=new ArrayList<>();
//        mol.addAll(pendientes);
//        for(int x=0; x<mol.size(); x++)
//        {
//            ArrayList<Integer>temp=new ArrayList<>();
//            for (int y=0; y<mol.size(); y++)
//            {
//                if(x!=y)
//                {
//                    if(mol.get(x).nombreCliente.equals(mol.get(y).nombreCliente))
//                    {
//                        temp.add(y);
//                    }
//                }
//            }
//            totK.add(mol.get(temp.get(0)));
//            temp.clear();
//        }
//        mol.clear();
//    }

    private synchronized void setUpGClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        mylocation = location;
        if (mylocation != null) {
            Double latitude=mylocation.getLatitude();
            Double longitude=mylocation.getLongitude();

        }
    }

    public void dialK(View v)
    {
        final int posi = lv.getPositionForView((View) v.getParent());
        //inflater
        LayoutInflater inf = LayoutInflater.from(this);
        //inflamos con un layout personalizado
        View prom = inf.inflate(R.layout.folo,null);
        final AlertDialog aler = new AlertDialog.Builder(this).create();

        TextView nombreFolo = prom.findViewById(R.id.nomFolo);
        TextView fechaFOlo = prom.findViewById(R.id.fechaFolo);
        TextView sumaTotalT = prom.findViewById(R.id.sumaTotal);
        TextView ObservacionFolo = prom.findViewById(R.id.observacion);
//        TextView valorFolo = prom.findViewById(R.id.valorFolo);
//        TextView pedidoFolo = prom.findViewById(R.id.pedidoFolo);
//        TextView saldoFolo = prom.findViewById(R.id.saldoFolo);

        //aler.setTitle("Detalles");
        aler.setCanceledOnTouchOutside(false);

        nombreFolo.setText(pendientes.get(posi).get("nombreCliente"));
        fechaFOlo.setText(pendientes.get(posi).get("fecha"));
        ObservacionFolo.setText(pendientes.get(posi).get("observacion"));
        double sumaTotal = 0.0;
//        jsonArray
        try {
            jsonObject = jsonArray.getJSONObject(posi);
            JSONArray det_kardex = jsonObject.getJSONArray("detalles_kardex");
            ArrayList<Category> prod = new ArrayList<Category>();
            for (int j=0; j<det_kardex.length(); j++){
                JSONObject producto = det_kardex.getJSONObject(j);
                JSONObject datoProducto = producto.getJSONObject("producto");


                String precio_unitario = producto.getString("precio_unitario");
                if (precio_unitario == "null"){
                    precio_unitario = datoProducto.getString("precio_unitario");
                }

                double cantidad = producto.getDouble("cantidad");
                double total = Double.parseDouble(precio_unitario)*cantidad;
                double saldo = producto.getDouble("saldo");
                double entregado = cantidad-saldo;
                double transporte = producto.getDouble("servicio_transporte");
                sumaTotal=sumaTotal+total;

                prod.add(new Category(datoProducto.getString("nombre"), producto.getInt("cantidad"), total, Double.parseDouble(precio_unitario), entregado, saldo, transporte));
            }

            sumaTotalT.setText(Double.toString(sumaTotal));
            ListView listaP = prom.findViewById(R.id.listaAlertaDetalle);
            AdapterItemDetalles po = new AdapterItemDetalles(this, prod);
            listaP.setAdapter(po);
            po.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }






//        valorFolo.setText("gg");
//        pedidoFolo.setText(pendientes.get(posi).get("id_usuario"));
//        saldoFolo.setText("fgg");

        aler.setButton(DialogInterface.BUTTON_POSITIVE, "Salir", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                aler.dismiss();
            }
        });
        aler.setButton(DialogInterface.BUTTON_NEGATIVE, "Guardar PDF", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //createPdf("ododo", "04-03-2018", "lolo");
//                desp = pendientes.get(posi);
                dale();
            }
        });

        aler.setView(prom);
        aler.show();
    }

    @Override
    public void onConnected(Bundle bundle) {
        checkPermissions();
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Do whatever you need
        //You can display a message here
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //You can display a message here
    }

    private void checkPermissions(){
        int permissionLocation = ContextCompat.checkSelfPermission(PedidoKardexFactura.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this,
                        listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            }
        }else{
            getMyLocation();
        }

    }

    private void getMyLocation(){
        if(googleApiClient!=null) {
            if (googleApiClient.isConnected()) {
                int permissionLocation = ContextCompat.checkSelfPermission(PedidoKardexFactura.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    mylocation =                     LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(3000);
                    locationRequest.setFastestInterval(3000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi
                            .requestLocationUpdates(googleApiClient, locationRequest, this);
                    PendingResult<LocationSettingsResult> result =
                            LocationServices.SettingsApi
                                    .checkLocationSettings(googleApiClient, builder.build());
                    result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                        @Override
                        public void onResult(LocationSettingsResult result) {
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.
                                    int permissionLocation = ContextCompat
                                            .checkSelfPermission(PedidoKardexFactura.this,
                                                    Manifest.permission.ACCESS_FINE_LOCATION);
                                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                                        mylocation = LocationServices.FusedLocationApi
                                                .getLastLocation(googleApiClient);
                                    }
                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.
                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        status.startResolutionForResult(PedidoKardexFactura.this,
                                                REQUEST_CHECK_SETTINGS_GPS);
                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied.
                                    // However, we have no way
                                    // to fix the
                                    // settings so we won't show the dialog.
                                    // finish();
                                    break;
                            }
                        }
                    });
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        int permissionLocation = ContextCompat.checkSelfPermission(PedidoKardexFactura.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            getMyLocation();
        }
    }

    private class LecturaTodosClientesEmpresa extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            return Functions.readJSONFeed(urls[0]);
        }

        protected void onPostExecute(String result) {
            try {
                // Locate the NodeList name
                jsonArray = new JSONArray(result);
                clientes=new ArrayList<>();
                Cliente clienteAproximado=new Cliente();
                clienteAproximado.distancia=9999999;
                for (int i = 0; i < jsonArray.length(); i++) {
                    jsonObject = jsonArray.getJSONObject(i);
                    Cliente cliente = new Cliente(jsonObject.getInt("id"),jsonObject.getString("razon_social"),jsonObject.getLong("nit"),jsonObject.getDouble("latitud"),jsonObject.getDouble("longitud"));
                    clientes.add(cliente);
                    Location locationCliente = new Location("");
                    locationCliente.setLatitude(cliente.latitud);
                    locationCliente.setLongitude(cliente.longitud);
                    double actualLatitud=mylocation.getLatitude();
                    double actualLongitud=mylocation.getLongitude();
                    double distancia=locationCliente.distanceTo(mylocation);
                    clienteAproximado=obtenerMenorDistancia(clienteAproximado.distancia,clienteAproximado,distancia,cliente);
                }
                establecerCliente(null,0,clienteAproximado.getId());
                nombreCliente.setText(clienteAproximado.toString());
                despacho.nombreCliente=clienteAproximado.toString();

            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
        }

        public Cliente obtenerMenorDistancia(double distancia1,Cliente c1,double distancia2,Cliente c2){
            Cliente res;
            if(distancia1<distancia2){
                res=c1;
                res.distancia=distancia1;
            }else{
                res=c2;
                res.distancia=distancia2;
            }
            return res;
        }
    }

    public void establecerCliente(AdapterView<?> parent,int position,int idCliente){
        if(idCliente==0){
            Cliente item = (Cliente)parent.getItemAtPosition(position);
            despacho.idCliente=item.getId();
            despacho.nombreCliente=item.toString();
            new LecturaCliente().execute(Functions.REST_URL +"clientes/"+item.getId());
        }else{
            despacho.idCliente=idCliente;
            new LecturaCliente().execute(Functions.REST_URL +"clientes/"+idCliente);
        }
        addFacturar.setEnabled(true);
    }


    private class LecturaCliente extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            return Functions.readJSONFeed(urls[0]);
        }

        protected void onPostExecute(String result) {
            try {
                // Locate the NodeList name
                jsonObject = new JSONObject(result);
                clientesRazon=new ArrayList<>();
                if (jsonObject.getJSONArray("clientes_razon").length()>0) {
                    for (int i = 0; i < jsonObject.getJSONArray("clientes_razon").length(); i++) {
                        JSONObject clienteRazonJson = jsonObject.getJSONArray("clientes_razon").getJSONObject(i);

                        ClienteRazon clienteRazon = new ClienteRazon(clienteRazonJson.getInt("id"), clienteRazonJson.getString("razon_social"), clienteRazonJson.getLong("nit"));

                        clientesRazon.add(clienteRazon);

                        ArrayAdapter<ClienteRazon> adapter = new ArrayAdapter<ClienteRazon>(PedidoKardexFactura.this,
                                android.R.layout.simple_spinner_item,
                                clientesRazon);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinnerFactura.setAdapter(adapter);

                    }
                }else{
//                    ClienteRazon clienteRazon = new ClienteRazon(0, "", 0);
//                    clientesRazon.add(clienteRazon);
//
//                    ArrayAdapter<ClienteRazon> adapter = new ArrayAdapter<ClienteRazon>(PedidoKardexFactura.this,
//                            android.R.layout.simple_spinner_item,
//                            clientesRazon);
//                    spinnerFactura.setAdapter(adapter);
                }
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
        }
    }




    private class LecturaClientesEmpresa extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            return Functions.readJSONFeed(urls[0]);
        }

        protected void onPostExecute(String result) {
            try {
                // Locate the NodeList name
                jsonArray = new JSONArray(result);
                if (jsonArray.length()>0) {
                    clientes=new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);
                        Cliente cliente = new Cliente(jsonObject.getInt("id"),jsonObject.getString("razon_social"),jsonObject.getLong("nit"));
                        clientes.add(cliente);

                        ArrayAdapter<Cliente> adapter =
                                new ArrayAdapter<Cliente>(PedidoKardexFactura.this, android.R.layout.simple_list_item_1,clientes);
                        nombreCliente.setAdapter(adapter);
                        nombreCliente.showDropDown();

                    }
                }else{
                    addCliente(String.valueOf(nombreCliente.getText()));
                }

            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public void addCliente(String nombre)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater layoutInflater = this.getLayoutInflater();
        final View dialog = layoutInflater.inflate(R.layout.cliente, null);
        builder.setView(dialog);
        final AlertDialog alertDialog = builder.create();

        //cosas
        final EditText addNombres = dialog.findViewById(R.id.nombreClienteR);
        addNombres.setText(nombre);
        final EditText addTelefono = dialog.findViewById(R.id.telefonoCliente);
        final EditText addContacto = dialog.findViewById(R.id.contactoCliente);
        Button cancelarAdd = dialog.findViewById(R.id.cancelarAdd);
        Button aceptarAdd = dialog.findViewById(R.id.aceptarAdd);

        cancelarAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        aceptarAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(addNombres.getText().toString().length() == 0)
                {
                    addNombres.setError("ingrese el nombre");
                    addNombres.requestFocus();
                }
                else {
                    new GuardarClienteNuevo().execute(addNombres.getText().toString(), addTelefono.getText().toString(), addContacto.getText().toString());
                    nombreCliente.setText(addNombres.getText().toString());
                    addFacturar.setEnabled(true);
                    Toast.makeText(PedidoKardexFactura.this, "El cliente se registro", Toast.LENGTH_SHORT).show();
                    alertDialog.dismiss();
                }
            }
        });
        alertDialog.show();
    }

    private class GuardarClienteNuevo extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... data) {
            JSONObject datos = new JSONObject();
            try {
                datos.put("razon_social",data[0]);
                datos.put("telefono",data[1]);
                datos.put("contacto",data[2]);
                JSONObject res = Functions.postJSONdata("clientes-pedido/"+id_empresa,datos);

//                String idCliente = String.valueOf(resultado.getJSONObject("id"));
                Log.e("el resultadooo", String.valueOf(res));
                String idCliente = res.getString("id");
                Log.e("el ideeeeeeeeee", String.valueOf(idCliente));
                despacho.idCliente=Integer.valueOf(idCliente);


                establecerCliente(null,0,Integer.valueOf(idCliente));
            } catch (JSONException ex) {
                //Logger.getLogger(EditPeymentConcept.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "";
        }
    }

    public void procedeX(View v)
    {
        final int posi = lv.getPositionForView((View)v.getParent());
        Intent i = new Intent(this, ProductosKardexFactura.class);
        i.putExtra("NUMEROX", posi);
        i.putExtra("GUARDADO_LOCAL", "0");
        i.putExtra("ID_EMPRESA",id_empresa);
        i.putExtra("ID_USUARIO",id_usuario);
        startActivity(i);
    }

    public void procedeEliminar(View v)
    {
        final int posi = lv.getPositionForView((View)v.getParent());

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater layoutInflater = this.getLayoutInflater();
        final View dialog = layoutInflater.inflate(R.layout.eliminar, null);
        builder.setView(dialog);
        final AlertDialog alertDialog = builder.create();

        //cosas
        final TextView addNombres = dialog.findViewById(R.id.nombreVendedor);
        final TextView nombreA = dialog.findViewById(R.id.nomFolo);
        addNombres.setText(pendientes.get(posi).get("nombreCliente")+" "+pendientes.get(posi).get("fecha"));
        Button cancelarAdd = dialog.findViewById(R.id.cancelarAdd);
        Button aceptarAdd = dialog.findViewById(R.id.aceptarAdd);

        double sumaTotal = 0.0;
//        jsonArray
        try {
            jsonObject = jsonArray.getJSONObject(posi);
            JSONArray det_kardex = jsonObject.getJSONArray("detalles_kardex");
            ArrayList<Category> prod = new ArrayList<Category>();
            for (int j=0; j<det_kardex.length(); j++){
                JSONObject producto = det_kardex.getJSONObject(j);
                double cantidad = producto.getDouble("cantidad");
                double saldo = producto.getDouble("saldo");
                double entregado = cantidad-saldo;

                sumaTotal=sumaTotal+entregado;

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (sumaTotal != 0){
            aceptarAdd.setVisibility(View.GONE);
            nombreA.setText("La venta ya tiene pedidos realizados");
        }
        cancelarAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        aceptarAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String id_pedido = pendientes.get(posi).get("id");
//
                new EliminarKardex().execute(id_pedido, "hola");
                pendientes.clear();
                alertDialog.dismiss();
                new GetKardex().execute(Functions.REST_URL +"gtm-despacho-kardex-factura/empresa/"+id_empresa+"/usuario/"+id_usuario+"/factura/"+true);

            }
        });
        alertDialog.show();

//        Toast.makeText(PedidoKardex.this,"llego a eliminarrrrrr  ", Toast.LENGTH_SHORT).show();
    }

    private class EliminarKardex extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... data) {
            Functions.deleteJSONdata("gtm-venta-kardex/"+data[0]);

            return "";
        }
    }
    public void verificar()
    {
        SharedPreferences coso = this.getSharedPreferences("KOL", MODE_PRIVATE);
        int cosox = coso.getInt("NUMERO",0);

        if(cosox==1)
        {
//            cargarKardex();
        }
    }
//    public void loadSharedPreferencesLogList()
//    {
//        Context context = this;
//        List<Despacho> callLog = new ArrayList<Despacho>();
//        SharedPreferences mPrefs = context.getSharedPreferences("LOL", context.MODE_PRIVATE);
//        Gson gson = new Gson();
//        String json = mPrefs.getString("myJson", "");
//        if (json.isEmpty()) {
//            callLog = new ArrayList<Despacho>();
//        } else {
//            Type type = new TypeToken<List<Kardex>>() {
//            }.getType();
//            callLog = gson.fromJson(json, type);
//        }
//        pendientes.clear();
//        pendientes.addAll(callLog);
//        lv.setAdapter(adapter);
//    }
//    public void cargarKardex()
//    {
//        ArrayList<Despacho> callLog = new ArrayList<Despacho>();
//        SharedPreferences mPrefs = this.getSharedPreferences("LOL", Context.MODE_PRIVATE);
//        Gson gson = new Gson();
//        String json = mPrefs.getString("ListaFinal", "");
//        if (json.isEmpty()) {
//            callLog = new ArrayList<Despacho>();
//        } else {
//            Type type = new TypeToken<ArrayList<Despacho>>() {
//            }.getType();
//            callLog = gson.fromJson(json, type);
//        }
//        pendientes.clear();
//        pendientes.addAll(callLog);
//        //lv.setAdapter(adapter);
//
//    }

    public void dale()
    {
        try {
            createPdfWrapper();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void createPdfWrapper() throws FileNotFoundException,DocumentException{

        int hasWriteStoragePermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (hasWriteStoragePermission != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!shouldShowRequestPermissionRationale(Manifest.permission.WRITE_CONTACTS)) {
                    showMessageOKCancel("Necesitas acceso a la memoria del dispositivo.",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                                REQUEST_CODE_ASK_PERMISSIONS);
                                    }
                                }
                            });
                    return;
                }

                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_CODE_ASK_PERMISSIONS);
            }
            return;
        }else {
            createPdf();
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("Aceptar", okListener)
                .setNegativeButton("Cancelar", null)
                .create()
                .show();
    }

    private void createPdf() throws FileNotFoundException, DocumentException {

        File docsFolder = new File(Environment.getExternalStorageDirectory() + "/Documents");
        if (!docsFolder.exists()) {
            docsFolder.mkdir();
            Log.i(TAG, "Directorio creado.");
        }

        Calendar calendar = Calendar.getInstance();
        pdfFile = new File(docsFolder.getAbsolutePath(),
                desp.nombreCliente+" "+calendar.get(Calendar.DAY_OF_MONTH)
                        +"-"+calendar.get(Calendar.MONTH)+"-"
                        +calendar.get(Calendar.YEAR)+"-"+"("+calendar.get(Calendar.HOUR)+":"+calendar.get(Calendar.MINUTE)+":"+
                        calendar.get(Calendar.SECOND)+")"+".pdf");
        OutputStream output = new FileOutputStream(pdfFile);
        com.itextpdf.text.Document document = new com.itextpdf.text.Document();
        PdfWriter.getInstance(document, output);
        document.open();
        //document.add(new Paragraph("TITULO DE LA COSA ESTA"));
        //document.addTitle("TITULO");
        //document.add(new Paragraph("Las cosas son cambiantes y cada cierto tiempo asdasdasdasd"));
        Paragraph nombre = new Paragraph(desp.nombreCliente);
        nombre.setAlignment(Element.ALIGN_CENTER);
        document.add(nombre);


        Paragraph fecha = new Paragraph("Fecha: "+desp.fecha);
        fecha.setAlignment(Element.ALIGN_LEFT);
        document.add(fecha);

        Paragraph valor = new Paragraph("Valor: "+desp.valor);
        fecha.setAlignment(Element.ALIGN_LEFT);
        document.add(valor);

        Paragraph pedido = new Paragraph("Pedido: "+desp.idCliente);
        fecha.setAlignment(Element.ALIGN_LEFT);
        document.add(pedido);

        Paragraph saldo = new Paragraph(desp.saldo);
        fecha.setAlignment(Element.ALIGN_CENTER);
        document.add(saldo);

        document.close();

        mContext=this;


        previewPdf();

    }


    private void previewPdf() {

        PackageManager packageManager = getPackageManager();
        Intent testIntent = new Intent(Intent.ACTION_VIEW);
        testIntent.setType("application/pdf");
        List list = packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY);
        if (list.size() > 0) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            Uri uri = Uri.fromFile(pdfFile);
            intent.setDataAndType(uri, "application/pdf");

            startActivity(intent);
        }else{
            Toast.makeText(this,"Descarga una app para poder visualizar el archivo.",Toast.LENGTH_SHORT).show();
        }
    }

    /*

    private void createPdf(String contenido, String fecha, String nombre){
        int permissionCheck = ContextCompat.checkSelfPermission(PedidoKardex.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if(permissionCheck != PackageManager.PERMISSION_GRANTED) {
            // ask permissions here using below code
            ActivityCompat.requestPermissions(PedidoKardex.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    20);
        }
        // create a new document
        PdfDocument document = new PdfDocument();

        // crate a page description
        PdfDocument.PageInfo pageInfo =
                new PdfDocument.PageInfo.Builder(500, 500, 1).create();

        // start a page
        PdfDocument.Page page = document.startPage(pageInfo);

        Canvas canvas = page.getCanvas();

        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawPaint(paint);

        paint.setColor(Color.BLACK);
        paint.setTextSize(10);
        canvas.drawText("Eraze una vez un elefante que gustaba de golpear a otros elefantes.\nUn dia lo mataron a trompazos.\nFIN", 10, 25, paint);
        for(short n=0; n<60; n++)
        {
            canvas.drawText("\nLinea "+n, 100, 225, paint);
        }

        // finish the page
        document.finishPage(page);

        /*
        // Create Page 2
        pageInfo = new PdfDocument.PageInfo.Builder(500, 500, 2).create();
        page = document.startPage(pageInfo);
        canvas = page.getCanvas();
        paint = new Paint();
        paint.setColor(Color.BLUE);
        canvas.drawCircle(200, 200, 100, paint);
        document.finishPage(page);

        // write the document content
        String targetPdf = Environment.getExternalStorageDirectory().getPath()+"/"+nombre+"_"+fecha+".pdf";
        File filePath = new File(targetPdf);
        try {
            document.writeTo(new FileOutputStream(filePath));
            Toast.makeText(this, "Exportado correctamente", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Ha ocurrido un error: " + e.toString(),
                    Toast.LENGTH_LONG).show();
        }

        // close the document
        document.close();
    }
    */

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }



}
