package xd.lotus.agil;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import models.Despacho;


public class AdapterAlert extends BaseAdapter {

    protected Activity activity;
    protected ArrayList<Despacho> items;

    public AdapterAlert(Activity activity, ArrayList<Despacho> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    public void clear() {
        items.clear();
    }

    public void addAll(ArrayList<Despacho> category) {
        for (int i =0 ; i < category.size(); i++) {
            items.add(category.get(i));
        }
    }

    @Override
    public Object getItem(int arg0) {
        return items.get(arg0);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.alertar_item, null);
        }

        Despacho dir = items.get(position);

        TextView nombre = v.findViewById(R.id.nombreAlerta);
        TextView codigo = v.findViewById(R.id.pedidoAlerta);
        codigo.setText(dir.observaciones);
        nombre.setText(dir.nombreCliente);

        return v;
    }

}