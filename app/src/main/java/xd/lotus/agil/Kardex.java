package xd.lotus.agil;


import java.util.ArrayList;

import models.DetalleDespacho;

public class Kardex {

    String nombre;
    String codigo;
    String observaciones;
    ArrayList<DetalleDespacho>list;

    public Kardex(String nombre, String codigo) {
        super();
        this.nombre = nombre;
        this.codigo = codigo;

    }
    public Kardex(String nombre, String observaciones, String codigo)
    {
        super();
        this.nombre=nombre;
        this.observaciones=observaciones;
        this.codigo = codigo;
    }
    public Kardex(String nombre, String observaciones, String codigo, ArrayList<DetalleDespacho>items)
    {
        super();
        this.nombre=nombre;
        this.observaciones=observaciones;
        this.codigo = codigo;
        this.list = items;
    }

    public Kardex() {

    }
    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;    }
    public String getObservaciones()
    {
        return observaciones;
    }
    public void setObservaciones(String observaciones)
    {
        this.observaciones=observaciones;
    }
    public  void setList(ArrayList<DetalleDespacho>list)
    {
        this.list = list;
    }
    public ArrayList<DetalleDespacho> getList()
    {
        return list;
    }
}

