package xd.lotus.agil;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.pdf.PdfDocument;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import android.os.AsyncTask;
import android.app.ProgressDialog;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import java.util.HashMap;
import java.util.Objects;
import java.text.SimpleDateFormat;
import android.widget.TableLayout;
import android.widget.TableRow.LayoutParams;
import android.graphics.Color;

import android.graphics.Typeface;

import android.widget.TableRow;

import models.Category;
import models.Despacho;
import models.DetalleDespacho;
import utils.Functions;
import android.widget.ProgressBar;

public class Alertar extends AppCompatActivity {

    ListView lv;
    AdapterAlert adapter;
    // URL to get contacts JSON

    private ProgressDialog pDialog;
//    ArrayList<Despacho>despachos;
    ArrayList<HashMap<String, String>> despachos;
    Despacho despacho;
    JSONObject jsonObject;
    JSONArray jsonArray;
    String id_empresa,id_usuario;
    private ProgressBar bar;

    private File pdfFile;
    final private int REQUEST_CODE_ASK_PERMISSIONS = 111;
    private static final String TAG = "CreadorDePDF";
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alertar);

        getSupportActionBar().setTitle("Alertas");

        despachos = new ArrayList<>();
        id_empresa= getIntent().getStringExtra("ID_EMPRESA");
        id_usuario= getIntent().getStringExtra("ID_USUARIO");

        bar = (ProgressBar) this.findViewById(R.id.progressBar);
        button = this.findViewById(R.id.cancelarAlertaBoton);

//        despacho= new Despacho();

//        llenar();

        lv = findViewById(R.id.listAlertas);

//        new GetDespachos().execute();
        Log.d("empresa : ", id_empresa);
        Log.d("usuariooo : ", id_usuario);
        new GetDespachos().execute(Functions.REST_URL +"gtm-detalle-despacho-alerta/empresa/"+id_empresa+"/inicio/0/fin/0/empleado/0/cliente/0/usuario/"+id_usuario);

        findViewById(R.id.cancelarAlertaBoton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(Alertar.this, Select.class);
                i.putExtra("ID_EMPRESA",id_empresa);
                i.putExtra("ID_USUARIO",id_usuario);
                startActivity(i);

            }
        });



    }

    private class GetDespachos extends AsyncTask<String, Void, String> {

        protected String doInBackground(String... urls) {
            return Functions.readJSONFeed(urls[0]);
        }
        @Override
        protected void onPreExecute() {
//            super.onPreExecute();
            // Showing progress dialog
//            pDialog = new ProgressDialog(Alertar.this);
//            pDialog.setMessage("Por favor espera\n...");
//            pDialog.setCancelable(false);
//            pDialog.show();
            bar.setVisibility(View.VISIBLE);
//            button.setLayoutParams(new LinearLayout.LayoutParams(120, 10, 1));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    250, 35, 1);
            params.gravity = Gravity.CENTER;
            button.setLayoutParams(params);

        }

        protected void onPostExecute(String result) {
            // Dismiss the progress dialog
//            if (pDialog.isShowing())
//                pDialog.dismiss();
            bar.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    250, 0, 1);
            params.gravity = Gravity.CENTER;
            button.setLayoutParams(params);
            try {
                // Locate the NodeList name
                jsonArray = new JSONArray(result);
                despachos=new ArrayList<>();
                String hola2 = String.valueOf(jsonArray.length());

                for (int i = 0; i < jsonArray.length(); i++) {
                    jsonObject = jsonArray.getJSONObject(i);

                    String id = jsonObject.getString("id");
                    JSONObject despacho = jsonObject.getJSONObject("despacho");
                    JSONObject cliente = despacho.getJSONObject("cliente");
                    JSONObject cliente_razon = despacho.getJSONObject("cliente_razon");
                    String nombre = cliente.getString("razon_social");
                    JSONObject destinos = despacho.getJSONObject("destino");
                    String destino = destinos.getString("destino");
                    String direccion = destinos.getString("direccion");
                    String factura = cliente_razon.getString("razon_social");
                    String nit = cliente_razon.getString("nit");
                    String fecha = jsonObject.getString("fecha");
                    // tmp hash map for single contact
                    HashMap<String, String> contact = new HashMap<>();
                    id_usuario= getIntent().getStringExtra("ID_USUARIO");

                    String observacion = despacho.getString("observacion");

                    String total = jsonObject.getString("importe");
                    JSONObject productos = jsonObject.getJSONObject("producto");
                    String nombreProducto = productos.getString("nombre");
                    String productoCantidad = jsonObject.getString("cantidad");
                    String productoPrecio = jsonObject.getString("precio_unitario");
                    String fechaEmision = jsonObject.getString("createdAt");

                    contact.put("id", id);
                    contact.put("nombreAlerta", nombre);
                    contact.put("destino", destino);
                    contact.put("direccion", direccion);
                    contact.put("factura", factura);
                    contact.put("nit", nit);
                    contact.put("observacion", observacion);
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
                    Date date = dateFormat.parse(fecha);
                    Date dateEmit = dateFormat.parse(fechaEmision);
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

                    contact.put("fecha", formatter.format(date));
                    contact.put("total", total);
                    contact.put("nombreProducto", nombreProducto);
                    contact.put("productoCantidad", productoCantidad);
                    contact.put("productoPrecio", productoPrecio);
                    contact.put("fechaEmision", formatter.format(dateEmit));

                    despachos.add(contact);

                    ListAdapter adapter = new SimpleAdapter(
                            Alertar.this, despachos,
                            R.layout.alertar_item, new String[]{"nombreAlerta", "id"}, new int[]{R.id.nombreAlerta,
                            R.id.pedidoAlerta});

                    lv.setAdapter(adapter);

                }
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
        }




//        @Override
//        protected Void doInBackground(Void... arg0) {
//            HttpHandler sh = new HttpHandler();
//
//            // Making a request to url and getting response
//            String jsonStr = sh.makeServiceCall(url);
//
//            Log.e(TAG, "Response from url: " + jsonStr);
//
//            if (jsonStr != null) try {
////                    JSONObject jsonObj = new JSONObject(jsonStr);
//
//                JSONArray contacts = new JSONArray(jsonStr);
//
//                // Getting JSON Array node
////                    JSONArray contacts = jsonObj.getJSONArray("contacts");
//
//                // looping through All Contacts
//                for (int i = 0; i < contacts.length(); i++) {
//                    JSONObject c = contacts.getJSONObject(i);
//
//                    String id = c.getString("id");
//                    JSONObject despacho = c.getJSONObject("despacho");
//                    JSONObject cliente = despacho.getJSONObject("cliente");
//                    String nombre = cliente.getString("razon_social");
//
//                    // tmp hash map for single contact
//                    HashMap<String, String> contact = new HashMap<>();
//
//
//                    // adding each child node to HashMap key => value
//                    contact.put("id", id);
//                    contact.put("nombreAlerta", nombre);
//                    despachos.add(contact);
//
//
//
//
//                }
//
//            } catch (final JSONException e) {
//                Log.e(TAG, "Json parsing error: " + e.getMessage());
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(getApplicationContext(),
//                                "Json parsing error: " + e.getMessage(),
//                                Toast.LENGTH_LONG)
//                                .show();
//                    }
//                });
//
//            }
//            else {
//                Log.e(TAG, "Couldn't get json from server.");
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(getApplicationContext(),
//                                "Couldn't get json from server. Check LogCat for possible errors!",
//                                Toast.LENGTH_LONG)
//                                .show();
//                    }
//                });
//
//            }
//
//            return null;
//        }

//        @Override
//        protected void onPostExecute(Void result) {
//            super.onPostExecute(result);
//            // Dismiss the progress dialog
//            if (pDialog.isShowing())
//                pDialog.dismiss();
//            /**
//             * Updating parsed JSON data into ListView
//             * */
//            ListAdapter adapter = new SimpleAdapter(
//                    Alertar.this, despachos,
//                    R.layout.alertar_item, new String[]{"nombreAlerta", "id"}, new int[]{R.id.nombreAlerta,
//                    R.id.pedidoAlerta});
//
//            lv.setAdapter(adapter);
//        }
    }


    public void editarX(View v)
    {
        final int posi = lv.getPositionForView((View)v.getParent());
        Intent i = new Intent(this, ProductosAlerta.class);
        i.putExtra("NUMEROX", posi);
        i.putExtra("GUARDADO_LOCAL", "0");
        i.putExtra("ID_EMPRESA",id_empresa);
        i.putExtra("ID_USUARIO",id_usuario);
        try {
            i.putExtra("ALERT", jsonArray.getJSONObject(posi).toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        startActivity(i);
    }



    public void detallarPedido(View v)
    {

        final int posi = lv.getPositionForView((View) v.getParent());

//        Toast.makeText(Alertar.this,"posicion  "+ despachos.get(posi).get("nombreProducto"), Toast.LENGTH_SHORT).show();
        //inflater
        LayoutInflater inf = LayoutInflater.from(this);
        //inflamos con un layout personalizado
        View prom = inf.inflate(R.layout.foloalertas,null);
        final AlertDialog aler = new AlertDialog.Builder(this).create();

        TextView destino = prom.findViewById(R.id.destinoAle);
        TextView direccion = prom.findViewById(R.id.direccionAle);
        TextView factura = prom.findViewById(R.id.facturaAle);
        TextView nit = prom.findViewById(R.id.nitAle);
        TextView observaciones = prom.findViewById(R.id.observacionesAle);
        TextView fecha = prom.findViewById(R.id.fechaAle);
        TextView total = prom.findViewById(R.id.totalAle);
        TextView producto = prom.findViewById(R.id.producto);
        TextView cantidad = prom.findViewById(R.id.cantidad);
        TextView precio = prom.findViewById(R.id.precio);
        TextView cliente = prom.findViewById(R.id.cliente);

        //aler.setTitle("Detalles");
        aler.setCanceledOnTouchOutside(false);

        destino.setText(despachos.get(posi).get("destino"));
        direccion.setText(despachos.get(posi).get("direccion"));
        factura.setText(despachos.get(posi).get("factura"));
        nit.setText(despachos.get(posi).get("nit"));
        observaciones.setText(despachos.get(posi).get("observacion"));

        fecha.setText(despachos.get(posi).get("fecha"));
        total.setText(despachos.get(posi).get("total"));
        producto.setText(despachos.get(posi).get("nombreProducto"));
        cantidad.setText(despachos.get(posi).get("productoCantidad"));
        precio.setText(despachos.get(posi).get("productoPrecio"));
        cliente.setText(despachos.get(posi).get("nombreAlerta"));




//        addHeaders(prom);
//        TableLayout tl = prom.findViewById(R.id.maintable);
//        TableRow tr = new TableRow(this);
//        tr.setLayoutParams(getLayoutParams());
//        tr.addView(getTextView(1, despachos.get(posi).get("nombreProducto"), Color.WHITE, Typeface.NORMAL, Color.BLUE,10, 20, 300));
//        tr.addView(getTextView(1, despachos.get(posi).get("productoCantidad"), Color.WHITE, Typeface.NORMAL, Color.BLUE, 10, 10, 200));
//        tr.addView(getTextView(1, despachos.get(posi).get("productoPrecio"), Color.WHITE, Typeface.NORMAL, Color.BLUE, 10, 10, 100));
//        tl.addView(tr, getTblLayoutParams());



        aler.setButton(DialogInterface.BUTTON_POSITIVE, "Salir", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                aler.dismiss();
            }
        });
        aler.setButton(DialogInterface.BUTTON_NEGATIVE, "Guardar PDF", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                stringtopdf(posi);
            }
        });

        aler.setView(prom);
        aler.show();

    }


    private TextView getTextView(int id, String title, int color, int typeface, int bgColor, int left, int right , int max) {
        TextView tv = new TextView(this);
        tv.setId(id);
        tv.setText(title.toUpperCase());
        tv.setTextColor(color);
        tv.setPadding(left, 10, right, 10);
        tv.setTypeface(Typeface.DEFAULT, typeface);
        tv.setBackgroundColor(bgColor);
        tv.setMaxWidth(max);
        tv.setLayoutParams(getLayoutParams());
        return tv;
    }
    @NonNull
    private LayoutParams getLayoutParams() {
        LayoutParams params = new LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT);
        params.setMargins(2, 0, 0, 2);
        return params;
    }
    @NonNull
    private TableLayout.LayoutParams getTblLayoutParams() {
        return new TableLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT);
    }


//    public void addHeaders(View prom) {
//        TableLayout tl = prom.findViewById(R.id.maintable);
//        TableRow tr = new TableRow(this);
//        tr.setLayoutParams(getLayoutParams());
//        tr.addView(getTextView(0, "PRODUCTO", Color.WHITE, Typeface.BOLD, Color.BLUE, 10, 20, 300));
//        tr.addView(getTextView(0, "CANTIDAD", Color.WHITE, Typeface.BOLD, Color.BLUE, 10, 10, 200));
//        tr.addView(getTextView(0, "PRECIO", Color.WHITE, Typeface.BOLD, Color.BLUE, 10, 10, 100));
//
//        tl.addView(tr, getTblLayoutParams());
//    }

    /**
     * This function add the data to the table
     **/

    public void stringtopdf(int posi) {
        String targetPdf =  Environment.getExternalStorageDirectory()
                +  "/pedido.pdf";
        File filePath = new File(targetPdf);

        try {

            FileOutputStream fOut = new FileOutputStream(filePath);

            PdfDocument document = new PdfDocument();
            PdfDocument.PageInfo pageInfo = new
                    PdfDocument.PageInfo.Builder(210, 297, 1).create();
            PdfDocument.Page page = document.startPage(pageInfo);
            Canvas canvas = page.getCanvas();
            Paint paint = new Paint();
            paint.setTextSize(9);
            paint.setShadowLayer(1f, 0f, 1f, Color.WHITE);

            Paint paintTitle = new Paint();
            paintTitle.setTextSize(9);
            paintTitle.setFlags(32);

            TextLayout(canvas, "Cliente:", despachos.get(posi).get("nombreAlerta"), 60, -170, paintTitle);
            TextLayout(canvas, "Fecha Emitida:", despachos.get(posi).get("fechaEmision"), 80, -130, paintTitle);
            TextLayout(canvas, "Destino:", despachos.get(posi).get("destino"), 60, -90, paintTitle);
            TextLayout(canvas, "Direccion:", despachos.get(posi).get("direccion"), 60, -50, paintTitle);
            TextLayout(canvas, "Factura:", despachos.get(posi).get("factura"), 60, -10, paintTitle);
            TextLayout(canvas, "NIT:", despachos.get(posi).get("nit"), 60, 30, paintTitle);
            TextLayout(canvas, "Observaciones:", despachos.get(posi).get("observacion"), 80, 70, paintTitle);
            TextLayout(canvas, "Fecha de entrega:", despachos.get(posi).get("fecha"), 90, 110, paintTitle);
            TextLayout(canvas, "Producto:", despachos.get(posi).get("nombreProducto"), 60, 150, paintTitle);
            TextLayout(canvas, "Cantidad:", despachos.get(posi).get("productoCantidad"), 60, 190, paintTitle);
            TextLayout(canvas, "Precio Unitario:", despachos.get(posi).get("productoPrecio"), 90, 230, paintTitle);
            TextLayout(canvas, "Total:", despachos.get(posi).get("total"), 50, 270, paintTitle);


            document.finishPage(page);
            document.writeTo(fOut);
            document.close();

            viewPdf(targetPdf);

        } catch (IOException e) {
            Log.i("error", "no lllegoooooooooo");
        }
    }

    public void TextLayout(Canvas canvas, String titulo, String texto, int left, int top, Paint paintTitle){
        Rect bounds = new Rect(left, top, 200, 200);
        TextPaint textPaint = new TextPaint();
        textPaint.setTextSize(9);

        StaticLayout sl = new StaticLayout(texto, textPaint, bounds.width(),
                Layout.Alignment.ALIGN_NORMAL, 1, 1, true);

        canvas.save();

        float textHeight = getTextHeight(texto, textPaint);
        int numberOfTextLines = sl.getLineCount();
        float textYCoordinate = bounds.exactCenterY() -
                ((numberOfTextLines * textHeight) / 2);

        //text will be drawn from left
        float textXCoordinate = bounds.left;
        canvas.drawText(titulo, 10, textYCoordinate+10, paintTitle);

        canvas.translate(textXCoordinate, textYCoordinate);

        //draws static layout on canvas
        sl.draw(canvas);
        canvas.restore();
    }

    private float getTextHeight(String text, Paint paint) {

        Rect rect = new Rect();
        paint.getTextBounds(text, 0, text.length(), rect);
        return rect.height();
    }


    // Method for opening a pdf file
    private void viewPdf(String filePath) {

        File pdfFile = new File( filePath);
        Uri path = Uri.fromFile(pdfFile);

        // Setting the intent for pdf reader
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(path, "application/pdf");
        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        try {
            startActivity(pdfIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "Can't read pdf file", Toast.LENGTH_SHORT).show();
        }
    }


    private class ActualizarPedido extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... data) {
            JSONObject datos = new JSONObject();
            try {
                datos.put("alerta",true);
                Functions.putJSONdata("gtm-detalle-despacho-alert/"+data[0],datos);

            } catch (JSONException ex) {
                //Logger.getLogger(EditPeymentConcept.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "";
        }
    }


    public void alertarPedido(View v) {
        final int posi = lv.getPositionForView((View) v.getParent());
        String id_pedido = despachos.get(posi).get("id");

        new ActualizarPedido().execute(id_pedido, "hola");

        Toast.makeText(this, "se envio la alerta¡", Toast.LENGTH_SHORT).show();



//
//        String id_despacho=despachos.get(posi).get("id");
//        // 1. create HttpClient
//        HttpClient httpclient = new DefaultHttpClient();
//        // 2. make POST request to the given URL
//        HttpPut httpPUT = new HttpPut("http://192.168.100.9:8083/agil/gtm-detalle-despacho-alert/1");
//        String json = "";
//        // 3. build jsonObject
//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("alerta",true);
//
//        // 4. convert JSONObject to JSON to String
//        json = jsonObject.toString();
//
//        // 5. set json to StringEntity
//        StringEntity se = new StringEntity(json);
//        // 6. set httpPost Entity
//        httpPUT.setEntity(se);
//        // 7. Set some headers to inform server about the type of the content
//
//        // 8. Execute POST request to the given URL
//
//        httpclient.execute(httpPUT);

    }
//    public void llenar()
//    {
//        //despachos.add(new Despacho(1 ,12, 12345678, "5/03/2018", "Sin factura", "Taxi Bolivar"));
//
//        despacho.nombreCliente="SERGIO FERNANDEZ CAMPERO";
//        despacho.fecha="5/3/2018";
//        despacho.observaciones="Sin factura, solo recibo";
//        despacho.idDestino=455;
//        despacho.idClienteRazon=12345678;
//        despacho.servicio_transporte="Transportes Carrasco";
//
//        despachos.add(despacho);
//    }

    public void dale()
    {
        try {
            createPdfWrapper();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    public void procedeEliminar(View v)
    {
        final int posi = lv.getPositionForView((View)v.getParent());

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater layoutInflater = this.getLayoutInflater();
        final View dialog = layoutInflater.inflate(R.layout.eliminar, null);
        builder.setView(dialog);
        final AlertDialog alertDialog = builder.create();

        //cosas
        final TextView addNombres = dialog.findViewById(R.id.nombreVendedor);
        final TextView nombreA = dialog.findViewById(R.id.nomFolo);
        addNombres.setText(despachos.get(posi).get("nombreAlerta")+" "+despachos.get(posi).get("id"));
        Button cancelarAdd = dialog.findViewById(R.id.cancelarAdd);
        Button aceptarAdd = dialog.findViewById(R.id.aceptarAdd);

        double sumaTotal = 0.0;

//        try {
//            jsonObject = jsonArray.getJSONObject(posi);
//            JSONArray det_kardex = jsonObject.getJSONArray("detalles_kardex");
//            ArrayList<Category> prod = new ArrayList<Category>();
//            for (int j=0; j<det_kardex.length(); j++){
//                JSONObject producto = det_kardex.getJSONObject(j);
//                double cantidad = producto.getDouble("cantidad");
//                double saldo = producto.getDouble("saldo");
//                double entregado = cantidad-saldo;
//
//                sumaTotal=sumaTotal+entregado;
//
//            }
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        if (sumaTotal != 0){
//            aceptarAdd.setVisibility(View.GONE);
//            nombreA.setText("La venta ya tiene pedidos realizados");
//        }
        cancelarAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        aceptarAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String id_pedido = despachos.get(posi).get("id");
//
                new EliminarAlerta().execute(id_pedido, "hola");
                despachos.clear();
                alertDialog.dismiss();
                // new GetDespachos().execute(Functions.REST_URL +"gtm-detalle-despacho/empresa/"+id_empresa+"/inicio/0/fin/0/empleado/0/cliente/0");
                new GetDespachos().execute(Functions.REST_URL +"gtm-detalle-despacho-alerta/empresa/"+id_empresa+"/inicio/0/fin/0/empleado/0/cliente/0/usuario/"+id_usuario);
                //                new GetKardex().execute(Functions.REST_URL +"gtm-despacho-kardex-factura/empresa/"+id_empresa+"/usuario/"+id_usuario+"/factura/"+true);

            }
        });
        alertDialog.show();

//        Toast.makeText(PedidoKardex.this,"llego a eliminarrrrrr  ", Toast.LENGTH_SHORT).show();
    }

    private class EliminarAlerta extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... data) {
            Functions.deleteJSONdata("gtm-detalle-despacho-alerta/"+data[0]);

            return "";
        }
    }

    private void createPdfWrapper() throws FileNotFoundException,DocumentException{

        int hasWriteStoragePermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (hasWriteStoragePermission != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!shouldShowRequestPermissionRationale(Manifest.permission.WRITE_CONTACTS)) {
                    showMessageOKCancel("Necesitas acceso a la memoria del dispositivo.",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                                REQUEST_CODE_ASK_PERMISSIONS);
                                    }
                                }
                            });
                    return;
                }

                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_CODE_ASK_PERMISSIONS);
            }
            return;
        }else {
            createPdf();
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("Aceptar", okListener)
                .setNegativeButton("Cancelar", null)
                .create()
                .show();
    }


    private void createPdf() throws FileNotFoundException, DocumentException {

        File docsFolder = new File(Environment.getExternalStorageDirectory() + "/Documents");
        if (!docsFolder.exists()) {
            docsFolder.mkdir();
            Log.i(TAG, "Directorio creado.");
        }

        Calendar calendar = Calendar.getInstance();
        pdfFile = new File(docsFolder.getAbsolutePath(),
                despacho.nombreCliente+"_"+calendar.get(Calendar.DAY_OF_MONTH)
                        +"-"+calendar.get(Calendar.MONTH)+"-"
                        +calendar.get(Calendar.YEAR)+"-"+"("+calendar.get(Calendar.HOUR)+":"+calendar.get(Calendar.MINUTE)+":"+
                        calendar.get(Calendar.SECOND)+")"+".pdf");
        OutputStream output = new FileOutputStream(pdfFile);
        com.itextpdf.text.Document document = new com.itextpdf.text.Document();
        PdfWriter.getInstance(document, output);
        document.open();
        //document.add(new Paragraph("TITULO DE LA COSA ESTA"));
        //document.addTitle("TITULO");
        //document.add(new Paragraph("Las cosas son cambiantes y cada cierto tiempo asdasdasdasd"));


        Paragraph nombre = new Paragraph(despacho.nombreCliente);
        nombre.setAlignment(Element.ALIGN_CENTER);
        document.add(nombre);


        Paragraph fecha = new Paragraph("Fecha: "+despacho.fecha);
        fecha.setAlignment(Element.ALIGN_LEFT);
        document.add(fecha);

        Paragraph valor = new Paragraph("Valor: "+despacho.valor);
        fecha.setAlignment(Element.ALIGN_LEFT);
        document.add(valor);

        Paragraph pedido = new Paragraph("Pedido: "+despacho.idCliente);
        fecha.setAlignment(Element.ALIGN_LEFT);
        document.add(pedido);

        Paragraph razon = new Paragraph("Razón social: "+despacho.idClienteRazon);
        fecha.setAlignment(Element.ALIGN_LEFT);
        document.add(razon);

        Paragraph observaciones = new Paragraph("Observaciones: "+despacho.observaciones);
        fecha.setAlignment(Element.ALIGN_LEFT);
        document.add(observaciones);

        document.close();


        previewPdf();

    }


    private void previewPdf() {

        PackageManager packageManager = getPackageManager();
        Intent testIntent = new Intent(Intent.ACTION_VIEW);
        testIntent.setType("application/pdf");
        List list = packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY);
        if (list.size() > 0) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            Uri uri = Uri.fromFile(pdfFile);
            intent.setDataAndType(uri, "application/pdf");

            startActivity(intent);
        }else{
            Toast.makeText(this,"Descarga una app para poder visualizar el archivo.",Toast.LENGTH_SHORT).show();
        }
    }
}
