package xd.lotus.agil;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import models.Despacho;
import models.DetalleDespacho;
import models.Producto;
import utils.Functions;

public class ProductosKardex extends AppCompatActivity {

    ProductoAdapter mAdapter;
    String id_empresa,id_usuario;
    ArrayList<Producto> productosList = new ArrayList<>();
    AdapterProductosKardex adapter;
    ArrayList<DetalleDespacho> category;
    int[] precios = {50, 250, 50, 250, 50, 250, 50, 250};
    TextView textView;
    ListView lv;
    ArrayList<Despacho> callLog;
    JSONObject jsonObject;
    JSONArray jsonArray;

    ArrayList<Despacho> sof = new ArrayList<>();


    String nombre;
    String guardadoLocal;
    String observaciones;
    Despacho despacho;
    int numero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productos_kardex);

        getSupportActionBar().setTitle("Productos");
        nombre = getIntent().getStringExtra("NOMBRE_KARDEX");
        guardadoLocal=getIntent().getStringExtra("GUARDADO_LOCAL");
        observaciones = getIntent().getStringExtra("OBS_KARDEX");
        id_empresa= getIntent().getStringExtra("ID_EMPRESA");
        id_usuario= getIntent().getStringExtra("ID_USUARIO");
        despacho=(Despacho)getIntent().getSerializableExtra("despacho");
        numero = getIntent().getIntExtra("NUMEROX", 0);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        final RecyclerView recyclerView = findViewById(R.id.resorK);
        recyclerView.setLayoutManager(linearLayoutManager);
        mAdapter = new ProductoAdapter(productosList);
        recyclerView.getLayoutManager().setMeasurementCacheEnabled(false);
        recyclerView.setAdapter(mAdapter);
        if (guardadoLocal.equals("1")) {
            new LecturaProductos().execute(Functions.REST_URL +"productos/empresa/"+id_empresa+"/pagina/1/items-pagina/10/busqueda/0/columna/nombre/direccion/asc/grupo/0/user/"+id_usuario);
        }




        textView = findViewById(R.id.textoteK);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // TODO Handle item click
                        //Toast.makeText(Productos.this, "XD "+position, Toast.LENGTH_SHORT ).show();
                        dialogador(productosList.get(position), position);
                    }
                })
        );

        category = new ArrayList<>();
        adapter = new AdapterProductosKardex(ProductosKardex.this, category, guardadoLocal);
        lv = findViewById(R.id.listaProductosK);
        lv.setAdapter(adapter);

//        loadSharedPreferencesLogList();

        if(guardadoLocal.equals("0")){
//            cargarKardeX();
            new cargarKardex().execute(Functions.REST_URL +"gtm-despacho-kardex-factura/empresa/"+id_empresa+"/usuario/"+id_usuario+"/factura/"+false);

        }


        Button aceptarTodo = findViewById(R.id.aceptarTodoK);
        Button cancelarTodo = findViewById(R.id.cancelarTodoK);

        aceptarTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarTodo();
            }
        });
        cancelarTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ProductosKardex.this, "Pedido cancelado", Toast.LENGTH_SHORT).show();
                Intent i=new Intent(ProductosKardex.this, PedidoKardex.class);
                i.putExtra("ID_EMPRESA",id_empresa);
                i.putExtra("ID_USUARIO",id_usuario);
                startActivity(i);
            }
        });

    }


    private class LecturaProductos extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            return Functions.readJSONFeed(urls[0]);
        }

        protected void onPostExecute(String result) {
            try {
                // Locate the NodeList name
                JSONObject jsonObject = new JSONObject(result);
                JSONArray productosJson = jsonObject.getJSONArray("productos");
                for (int i = 0; i < productosJson.length(); i++) {
                    JSONObject productoJson = productosJson.getJSONObject(i);
                    Producto producto=new Producto(productoJson.getInt("id"),productoJson.getString("nombre"),productoJson.getString("imagen"),0,productoJson.getDouble("precio_unitario"));

                    productosList.add(producto);
                }
                mAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public void dialogador(final Producto producto, final int pos)
    {
        final String nom=producto.getNombre();
        //inflater
        LayoutInflater inf = LayoutInflater.from(this);
        //inflamos con un layout personalizado
        View prom = inf.inflate(R.layout.roke,null);
        final AlertDialog aler = new AlertDialog.Builder(this).create();

        aler.setTitle("Producto");

//        TextView txtNom = prom.findViewById(R.id.nombreProducto);
        final EditText edCant = prom.findViewById(R.id.cantidadProducto);
        final EditText edPrec = prom.findViewById(R.id.precioFinalProducto);
        final EditText trans = prom.findViewById(R.id.transporte);
        final EditText totaltrans = prom.findViewById(R.id.totaltransporte);
        totaltrans.setEnabled(false);


        trans.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String dtrans = "0";
                if (s.length() > 0){
                    dtrans = s.toString();
                }
                double caltransporte = Double.parseDouble(edCant.getText().toString()) * Double.parseDouble(dtrans);

                totaltrans.setText(String.valueOf(caltransporte));
            }
        });

        Button canCant = prom.findViewById(R.id.canCantidad);
        Button acepCantidad = prom.findViewById(R.id.acepCantidad);

        edCant.append("1");
        edPrec.setText(producto.getPrecio().toString());

        trans.append("0");

        edCant.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String dcant = "0";
                if (s.length() > 0){
                    dcant = s.toString();
                }
                double caltransporte2 = Double.parseDouble(dcant) * Double.parseDouble(trans.getText().toString());

                totaltrans.setText(String.valueOf(caltransporte2));
            }
        });

//        txtNom.setText(nom);
        canCant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aler.dismiss();
            }
        });
        acepCantidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edCant.length()==0)
                {
                    aler.dismiss();
                }
                else {
                    double total = Double.parseDouble(edPrec.getText().toString())*Double.parseDouble(edCant.getText().toString());
                    int cantidad = Integer.parseInt(edCant.getText().toString());
                    category.add(new DetalleDespacho(producto.getId(),nom, Integer.toString(cantidad), edPrec.getText().toString(), Double.toString(total), trans.getText().toString()));
                    adapter.notifyDataSetChanged();
                    sumar();
                    aler.dismiss();

                }
            }
        });

        aler.setView(prom);
        aler.show();
    }

    public void sumar()
    {
        double te= 0;
        for(int i = 0; i<category.size(); i++)
        {
            te+=Double.parseDouble(category.get(i).getPrecioTotal());
        }
        textView.setText(Double.toString(te));
    }


    public void eliminarX(View v) {
        final int position = lv.getPositionForView((View) v.getParent());
        restar(position);
        category.remove(position);
        lv.setAdapter(adapter);
        //adapter.notifyDataSetChanged();

    }

    public void restar(int p) {
        double total = Double.parseDouble(textView.getText().toString());
        double sus = Double.parseDouble(category.get(p).getPrecioTotal());
        total= total-sus;
        textView.setText(Double.toString(total));
    }

    public void editarX(View v) {
        final int posi = lv.getPositionForView((View) v.getParent());
        restar(posi);
        editor(posi);
    }

    /*public void guardarEditado(String nombre, int cantidad, int precio, int pos) {
        category.set(pos, new DetalleDespacho(nombre, Integer.toString(cantidad), Integer.toString(precio)));
        adapter.notifyDataSetChanged();
        sumar();
    }*/

    public void editor(final int eje)
    {
        //inflater
        LayoutInflater inf = LayoutInflater.from(this);
        //inflamos con un layout personalizado
        View prom = inf.inflate(R.layout.roke,null);
        final AlertDialog aler = new AlertDialog.Builder(this).create();

        aler.setTitle("Producto");

//        TextView txtNom = prom.findViewById(R.id.nombreProducto);
        final EditText edCant = prom.findViewById(R.id.cantidadProducto);
        final EditText edPrec = prom.findViewById(R.id.precioFinalProducto);
        final Button canCant = prom.findViewById(R.id.canCantidad);
        final EditText trans = prom.findViewById(R.id.transporte);
        final EditText totaltrans = prom.findViewById(R.id.totaltransporte);
        totaltrans.setEnabled(false);
        Button acepCantidad = prom.findViewById(R.id.acepCantidad);

        //edCant.setSelection(edCant.length());
        edCant.setText(category.get(eje).getCantidad());
        //Toast.makeText(Productos.this, edCant.getText()+"////////sdfsdfs", Toast.LENGTH_SHORT).show();
        edPrec.setText(category.get(eje).getPrecioUnitario());

        trans.setText(category.get(eje).getTransporte());

        double caltrans = Double.parseDouble(category.get(eje).getCantidad())*Double.parseDouble(category.get(eje).getTransporte());
        totaltrans.setText(String.valueOf(caltrans));

        if (guardadoLocal.equals("0")) {
            trans.setEnabled(false);
        }

        trans.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String dtrans = "0";
                if (s.length() > 0){
                    dtrans = s.toString();
                }
                double caltransporte = Double.parseDouble(edCant.getText().toString()) * Double.parseDouble(dtrans);

                totaltrans.setText(String.valueOf(caltransporte));
            }
        });

        edCant.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String dcant = "0";
                if (s.length() > 0){
                    dcant = s.toString();
                }
                double caltransporte2 = Double.parseDouble(dcant) * Double.parseDouble(trans.getText().toString());

                totaltrans.setText(String.valueOf(caltransporte2));
            }
        });

        final String noma = category.get(eje).getNombre();
//        txtNom.setText(noma);
        canCant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sumar();
                aler.dismiss();
            }
        });
        acepCantidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edCant.length()==0)
                {
                    aler.dismiss();
                }
                else {
//                    === validar si la cantidad es menor o igual al saldo ===
                    double cantidad = Double.parseDouble(edCant.getText().toString());

                    boolean validar  = true;
                    if (guardadoLocal.equals("0")) {
                        JSONObject datok = null;
                        try {
                            datok = jsonArray.getJSONObject(numero);
                            JSONArray kardex = datok.getJSONArray("detalles_kardex");
                            Double saldo = Double.parseDouble(kardex.getJSONObject(eje).getString("saldo"));
                            Log.e("los datos saldooo", String.valueOf(saldo));

                            if (cantidad > saldo){
                                edCant.setError("la cantidad es mayor al saldo " + saldo);
                                validar = false;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    if (validar){
                        category.get(eje).setCantidad(cantidad+"");
                        //Double.parseDouble(category.get(eje).getPrecioTotal())
                        category.get(eje).setPrecioTotal(Double.toString(cantidad * Double.parseDouble(edPrec.getText().toString())));

                        category.get(eje).setTransporte(trans.getText().toString());
                        //adapter.notifyDataSetChanged();
                        lv.setAdapter(adapter);
                        sumar();
                        aler.dismiss();
                    }

                }
            }
        });

        aler.setView(prom);
        aler.show();
    }
    public  void GuardarEditar(){

    }

    public void guardarTodo()
    {
        int dia, mes, ano;
        Calendar calendar = Calendar.getInstance();
        dia = calendar.get(Calendar.DAY_OF_MONTH);
        mes= calendar.get(Calendar.MONTH);
        ano= calendar.get(Calendar.YEAR);

        despacho.setDetalleDespacho(category);
        despacho.id_usuario=id_usuario;
        despacho.fecha=Integer.toString(dia)
                +"/"+Integer.toString(mes)+"/"
                +Integer.toString(ano);
        despacho.valor=textView.getText().toString();
        if (despacho.detallesDespacho.size()>0) {
            if(guardadoLocal.equals("1")){
//                saveSharedPreferencesLogList();
//                para guardar los nuevos pedidos en kardex ========
                new GuardarKardex().execute();
                Toast.makeText(ProductosKardex.this, "Pedido exitoso", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(ProductosKardex.this, Select.class);
                i.putExtra("ID_EMPRESA",id_empresa);
                i.putExtra("ID_USUARIO",id_usuario);
                startActivity(i);
            }else{
//                para hacer el pedido despues del kardex ======
                Toast.makeText(ProductosKardex.this, "Pedido exitoso", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(ProductosKardex.this, Pedido.class);
                i.putExtra("ID_EMPRESA",id_empresa);
                i.putExtra("ID_USUARIO",id_usuario);
                i.putExtra("despacho",new Despacho(despacho));
                i.putExtra("GUARDADO_LOCAL","1");
                i.putExtra("GUARDADO_FACTURA","0");
                i.putExtra("NUMEROX",numero);
                startActivity(i);
            }
        }else{
//            Toast.makeText(Productos.this, "¡Debe agregar al menos un producto para realizar la transacción!", Toast.LENGTH_SHORT).show();
            Toast toast = Toast.makeText(ProductosKardex.this, "¡Debe agregar al menos un producto!", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }


    public void saveSharedPreferencesLogList()
    {
        //ordenar();
//        guardar en la base de datos
        sof.add(despacho);
        Context context = this;
        SharedPreferences mPrefs = context.getSharedPreferences("LOL", context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(sof);
        prefsEditor.putString("ListaFinal", json);
        prefsEditor.commit();

    }

    private class GuardarKardex extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... data) {
            despacho.setDetalleDespacho(category);
            despacho.id_usuario=id_usuario;
            JSONObject datos = new JSONObject();
            try {
                datos.put("id_empresa",id_empresa);
                datos.put("id_usuario",despacho.id_usuario);
                datos.put("fecha",despacho.fecha);
                datos.put("id_cliente",despacho.idCliente);
                datos.put("observacion", despacho.observaciones);
//                datos.put("id_cliente_razon",despacho.idClienteRazon);
                datos.put("factura",false);


                JSONArray detallesDespacho=new JSONArray();

                for (int i = 0; i < despacho.detallesDespacho.size(); i++) {
                    JSONObject jsonobj = new JSONObject();
                    jsonobj.put("id_producto", despacho.detallesDespacho.get(i).id_producto);
                    jsonobj.put("cantidad", despacho.detallesDespacho.get(i).cantidad);
                    jsonobj.put("precio_unitario", despacho.detallesDespacho.get(i).precioUnitario);
                    jsonobj.put("servicio_transporte", despacho.detallesDespacho.get(i).transporte);
                    detallesDespacho.put(jsonobj);
                }

                datos.put("detalles_kardex", detallesDespacho);
                Functions.postJSONdata("gtm-despacho-kardex/empresa/" + id_empresa, datos);


            } catch (JSONException ex) {
                //Logger.getLogger(EditPeymentConcept.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "";
        }
    }

    public void loadSharedPreferencesLogList()
    {
        Context context = this;
        List<Despacho> callLog;
        SharedPreferences mPrefs = context.getSharedPreferences("LOL", context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("ListaFinal", "");
        if (json.isEmpty()) {
            callLog = new ArrayList<Despacho>();
        } else {
            Type type = new TypeToken<List<Despacho>>() {
            }.getType();
            callLog = gson.fromJson(json, type);
        }
        sof.addAll(callLog);


    }

//    public void cargarKardeX()
//    {
//        SharedPreferences mPrefs = this.getSharedPreferences("LOL", Context.MODE_PRIVATE);
//        Gson gson = new Gson();
//        String json = mPrefs.getString("ListaFinal", "");
//        if (json.isEmpty()) {
//            callLog = new ArrayList<Despacho>();
//        } else {
//            Type type = new TypeToken<ArrayList<Despacho>>() {
//            }.getType();
//            callLog = gson.fromJson(json, type);
//        }
//        //sof.addAll(callLog.get(numero));
//        despacho=callLog.get(numero);
//        for(int i=0;i<despacho.detallesDespacho.size();i++){
//            double total = Double.parseDouble(despacho.detallesDespacho.get(i).getPrecioUnitario())*Double.parseDouble(despacho.detallesDespacho.get(i).cantidad);
//            double cantidad = Double.parseDouble(despacho.detallesDespacho.get(i).cantidad);
//            category.add(new DetalleDespacho(despacho.detallesDespacho.get(i).id_producto,despacho.detallesDespacho.get(i).getNombre(), cantidad+"",despacho.detallesDespacho.get(i).getPrecioUnitario().toString(), Double.toString(total), despacho.detallesDespacho.get(i).getTransporte()));
//            adapter.notifyDataSetChanged();
//            sumar();
//        }
//    }

    private class cargarKardex extends AsyncTask<String, Void, String> {

        protected String doInBackground(String... urls) {
            return Functions.readJSONFeed(urls[0]);
        }

        protected void onPostExecute(String result) {

            try {

//                descomponer el resultado y mandarlo al array modelo de pendientes =======================
//                =========================================================================================

                jsonArray = new JSONArray(result);

                JSONObject dato = jsonArray.getJSONObject(numero);
                JSONArray kardex = dato.getJSONArray("detalles_kardex");
                JSONObject cliente = dato.getJSONObject("cliente");
//                despacho=callLog.get(numero);
//                agragar datos al modelo despachos ======================0000
//
                despacho=new Despacho();
                despacho.nombreCliente= cliente.getString("razon_social");
                despacho.idCliente = dato.getInt("id_cliente");
                for (int i = 0; i < kardex.length(); i++) {
                    Log.d("dato22: ", String.valueOf(i));
                    jsonObject = kardex.getJSONObject(i);
                    JSONObject producto = jsonObject.getJSONObject("producto");
                    String precio_unitario = jsonObject.getString("precio_unitario");
                    if (precio_unitario == "null"){
                        precio_unitario = producto.getString("precio_unitario");
                    }
                    double total = Double.parseDouble(precio_unitario)*Double.parseDouble(jsonObject.getString("saldo"));
                    double cantidad = Double.parseDouble(jsonObject.getString("saldo"));

                    category.add(new DetalleDespacho(jsonObject.getInt("id_producto"),producto.getString("nombre"), cantidad+"",precio_unitario, Double.toString(total), jsonObject.getString("servicio_transporte")));

                    adapter.notifyDataSetChanged();
                    sumar();
                }
//                despacho.detallesDespacho.addAll(category);

            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
        }

    }

    public void coso(int n)
    {
        SharedPreferences coso = this.getSharedPreferences("KOL", MODE_PRIVATE);
        SharedPreferences.Editor edit = coso.edit();
        edit.putInt("NUMERO",n);
        edit.commit();
    }


    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }


}
