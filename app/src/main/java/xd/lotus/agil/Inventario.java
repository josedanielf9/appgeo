package xd.lotus.agil;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import models.Despacho;

public class Inventario extends AppCompatActivity
{
    String clientex="";
    String fecha ="";
    String facturar="";
    String nit ="";
    String destino="";
    String direccion="";
    String obs="";
    String limite="";
    Despacho despacho;
    String id_empresa,id_usuario;

    String temp="";
    double latitud;
    double longitud;





    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventario);
        clientex = getIntent().getStringExtra("KEY_CLIENTE");
        fecha = getIntent().getStringExtra("KEY_DESPACHO");
        facturar= getIntent().getStringExtra("KEY_FACTURAR");
        nit= getIntent().getStringExtra("KEY_NIT");
        destino= getIntent().getStringExtra("KEY_DESTINO");
        direccion= getIntent().getStringExtra("KEY_DIRECCION");
        obs= getIntent().getStringExtra("KEY_OBS");
        despacho=(Despacho)getIntent().getSerializableExtra("despacho");
        id_empresa= getIntent().getStringExtra("ID_EMPRESA");
        id_usuario= getIntent().getStringExtra("ID_USUARIO");

        Log.d("longitud", String.valueOf(getIntent().getDoubleExtra("latitud", 0.00)));

        if(this.getIntent().getStringExtra("ACTIVITY") != null)
        {
            limite = this.getIntent().getStringExtra("ACTIVITY");
        }
        else{limite = "OLR";}





        getSupportActionBar().setTitle("Datos de pedido");



        TextView  cliente = findViewById(R.id.cliente);
        cliente.setText(clientex);
        TextView Tdestino = findViewById(R.id.Tdestino);
        Tdestino.setText(destino);
        TextView Tdireccion = findViewById(R.id.Tdireccion);
        Tdireccion.setText(direccion);
        TextView Tfactura = findViewById(R.id.Tfactura);
        Tfactura.setText(facturar);
        TextView Tnit = findViewById(R.id.Tnit);
        Tnit.setText(nit);
        TextView Tfecha = findViewById(R.id.Tfecha);
        Tfecha.setText(fecha);
        TextView Tobs = findViewById(R.id.Tobs);
        Tobs.setText(obs);

        Tdireccion.setMovementMethod(ScrollingMovementMethod.getInstance());
        Tobs.setMovementMethod(ScrollingMovementMethod.getInstance());

        Button editar = findViewById(R.id.editarDatos);
        editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Button siguiente = findViewById(R.id.sigInv);
        siguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(limite.equals("Limite"))
                    {
                        //Toast.makeText(Inventario.this, limite, Toast.LENGTH_SHORT).show();
                        Toast.makeText(Inventario.this, "Envio de pedido en proceso", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(Inventario.this, Select.class));
                    }
                    else
                    {
                        //Toast.makeText(Inventario.this, activity, Toast.LENGTH_SHORT).show();
                        Intent i=new Intent(Inventario.this, Productos.class);

                        i.putExtra("despacho", despacho);
                        i.putExtra("ID_EMPRESA",id_empresa);
                        i.putExtra("ID_USUARIO",id_usuario);
                        latitud = getIntent().getDoubleExtra("latitud", 0);
                        longitud = getIntent().getDoubleExtra("longitud", 0);
                        i.putExtra("latitud", latitud);
                        i.putExtra("longitud", longitud);
                        startActivity(i);
                    }

            }
        });

        Button cancelar = findViewById(R.id.cancelar);
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Inventario.this, "Cancelado", Toast.LENGTH_SHORT).show();
                Intent i=new Intent(Inventario.this, Select.class);
                i.putExtra("ID_EMPRESA",id_empresa);
                i.putExtra("ID_USUARIO",id_usuario);
                startActivity(i);
            }
        });
    }


}

