package xd.lotus.agil;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import models.Category;


public class AdapterItemDetalles extends BaseAdapter{
    protected Activity activity;
    protected ArrayList<Category> items;

    public AdapterItemDetalles(Activity activity, ArrayList<Category> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    public void clear() {
        items.clear();
    }

    public void addAll(ArrayList<Category> category) {
        for (int i = 0; i < category.size(); i++) {
            items.add(category.get(i));
        }
    }

    @Override
    public Object getItem(int arg0) {
        return items.get(arg0);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewHolder2 vh;
        //Category dir;
        if (convertView == null) {
            vh = new ViewHolder2();
            convertView = inf.inflate(R.layout.detallelistas, null);

            vh.producto = convertView.findViewById(R.id.nombreS);
            vh.cantidad = convertView.findViewById(R.id.cantidadS);
            vh.precio = convertView.findViewById(R.id.precioS);
            vh.total = convertView.findViewById(R.id.totalS);
            vh.entregado = convertView.findViewById(R.id.entregado);
            vh.saldo = convertView.findViewById(R.id.saldo);
            vh.transporte = convertView.findViewById(R.id.transporteS);
            vh.totalg = convertView.findViewById(R.id.totalG);

            convertView.setTag(vh);
        } else {
            vh = (ViewHolder2) convertView.getTag();
        }

        vh.producto.setText(items.get(position).getProducto());
        vh.cantidad.setText("Cant: "+Integer.toString(items.get(position).getCantidad()));
        vh.precio.setText("P/U: "+Double.toString(items.get(position).getPrecioUnitario()));
        vh.total.setText("Total: "+Double.toString(items.get(position).getPrecioTotal()));
        vh.entregado.setText("Pedido: "+Double.toString(items.get(position).getEntregado()));
        vh.saldo.setText("Saldo: "+Double.toString(items.get(position).getSaldo()));
        vh.transporte.setText("Transporte: "+Double.toString(items.get(position).getTransporte()));
        double totalg;
        totalg = Double.valueOf(items.get(position).getCantidad()) * items.get(position).getTransporte();

        vh.totalg.setText("TOTAL GENERAL: "+Double.toString(items.get(position).getPrecioTotal()+totalg));

        return convertView;
    }

    static class ViewHolder2 {
        TextView producto;
        TextView cantidad;
        TextView precio;
        TextView total;
        TextView entregado;
        TextView saldo;
        TextView transporte;
        TextView totalg;
    }
}