package xd.lotus.agil;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import models.DetalleDespacho;


public class AdapterItem extends BaseAdapter {

    protected Activity activity;
    protected ArrayList<DetalleDespacho> items;

    public AdapterItem (Activity activity, ArrayList<DetalleDespacho> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    public void clear() {
        items.clear();
    }

    public void addAll(ArrayList<DetalleDespacho> category) {
        for (int i =0 ; i < category.size(); i++) {
            items.add(category.get(i));
        }
    }

    @Override
    public Object getItem(int arg0) {
        return items.get(arg0);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            TextView nombre = null;
            TextView cantidad=null;
            TextView precio=null;
            DetalleDespacho dir = items.get(position);
                v = inf.inflate(R.layout.item_category, null);

                nombre = v.findViewById(R.id.nombre);

                cantidad = v.findViewById(R.id.cantidad);

                if(!(activity instanceof ProductosLimite))
                {
                    precio = v.findViewById(R.id.precio);
                    precio.setText(dir.getPrecioTotal());
                }

            nombre.setText(dir.getNombre());
            cantidad.setText(dir.getCantidad());


        }
        return v;
    }
}