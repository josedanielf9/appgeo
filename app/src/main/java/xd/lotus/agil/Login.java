package xd.lotus.agil;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import utils.Functions;

public class Login extends AppCompatActivity {

    EditText usuario;
    EditText contrasena;
    ProgressDialog cargando;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        Button ingresar = findViewById(R.id.ingresar);
        ingresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cargando = ProgressDialog.show(Login.this, "",
                "Conectando con el servidor, por favor, espere.", true);
                cargando.setCancelable(false);

                new IniciarSesion().execute();
                //startActivity(new Intent(Login.this, Select.class));
            }
        });
        ImageView geo = findViewById(R.id.botongeo);
        geo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cargando = ProgressDialog.show(Login.this, "",
                 "Conectando con el servidor, por favor, espere.", true);
                cargando.setCancelable(false);

                new IniciarSesion().execute();
                //startActivity(new Intent(Login.this, Select.class));
            }
        });

        usuario = findViewById(R.id.usuario);
        contrasena = findViewById(R.id.contrasena);
    }

    private class IniciarSesion extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... data) {
            String res = null;
            try {
                res=Functions.iniciarSesion(usuario.getText().toString(),contrasena.getText().toString());

            } catch (Exception ex) {
                //Logger.getLogger(EditPeymentConcept.class.getName()).log(Level.SEVERE, null, ex);
                Log.e("error", ex.getMessage());
            }
            return res;
        }

        protected void onPostExecute(String r) {
            try{
                JSONObject res=new JSONObject(r);
                boolean type=Boolean.parseBoolean(res.get("type").toString());
                if(type){
                    String id_empresa=res.getJSONObject("data").getString("id_empresa");
                    String id_usuario=res.getJSONObject("data").getString("id");
                    Intent i = new Intent(Login.this, Select.class);
                    i.putExtra("ID_EMPRESA",id_empresa);
                    i.putExtra("ID_USUARIO",id_usuario);
                    Toast.makeText(Login.this, "Correcto", Toast.LENGTH_SHORT).show();
                    cargando.dismiss();
                    startActivity(i);
                }else{
                    Toast.makeText(Login.this, res.get("data").toString(), Toast.LENGTH_SHORT).show();
                    cargando.dismiss();
                }
            }catch(Exception e){

            }
        }
    }
}
