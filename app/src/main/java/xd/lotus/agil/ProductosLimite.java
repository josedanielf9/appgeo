package xd.lotus.agil;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import models.Despacho;
import models.DetalleDespacho;
import models.Producto;

public class ProductosLimite extends AppCompatActivity {

    ProductoAdapter mAdapter;
    ArrayList<Producto> productList;
    AdapterItem adapter;
    ArrayList<DetalleDespacho> category;
    TextView textView ;
    ListView lv;

    ArrayList<DetalleDespacho>sof;
    ArrayList<Despacho> callLog;

    int numero;

    String nombre, observaciones,codigo; // y sof

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productos_limite);

        getSupportActionBar().setTitle("Productos en Kardex");

        numero = getIntent().getIntExtra("NUMEROX", 0);
        productList = new ArrayList<>();
        textView = findViewById(R.id.textoteL);

        sof = new ArrayList<>();
        callLog = new ArrayList<Despacho>();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        final RecyclerView recyclerView = findViewById(R.id.resorL);
        recyclerView.setLayoutManager(linearLayoutManager);
        mAdapter = new ProductoAdapter(productList);
        recyclerView.getLayoutManager().setMeasurementCacheEnabled(false);
        recyclerView.setAdapter(mAdapter);
        prepareMovieData();



        textView = findViewById(R.id.textoteL);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // TODO Handle item click
                        //Toast.makeText(Productos.this, "XD "+position, Toast.LENGTH_SHORT ).show();
                        dialogador(productList.get(position).getNombre(), position);
                    }
                })
        );

        category = new ArrayList<>();
        adapter = new AdapterItem(ProductosLimite.this, category);
        lv = findViewById(R.id.listaProductosL);
        lv.setAdapter(adapter);

        //loadSharedPreferencesLogList();
        cargarKardeX();

        Button aceptarTodo = findViewById(R.id.aceptarTodoL);
        Button cancelarTodo = findViewById(R.id.cancelarTodoL);

        aceptarTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarTodo();
            }
        });
        cancelarTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ProductosLimite.this, "Pedido cancelado", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(ProductosLimite.this, Select.class));
            }
        });

    }


    private void prepareMovieData()
    {
        productList.add(new Producto(1,"Cemento (rojo)","dsffds",1,1));
        productList.add(new Producto(1,"Cemento (rojo)","dsffds",1,1));

        mAdapter.notifyDataSetChanged();
    }
    public void dialogador(final String nom, int pos)
    {

        //inflater
        LayoutInflater inf = LayoutInflater.from(this);
        //inflamos con un layout personalizado
        View prom = inf.inflate(R.layout.picker,null);
        final AlertDialog aler = new AlertDialog.Builder(this).create();

        aler.setTitle("Producto");

        final NumberPicker np = prom.findViewById(R.id.numberPicker);
        Button cancelar = prom.findViewById(R.id.cancelar);
        Button ok = prom.findViewById(R.id.ok);

        int max=0;

        for(int i=0; i<sof.size(); i++)
        {
            if(sof.get(i).getNombre().equals(nom))
            {
                pos = i;
                if(Integer.parseInt(sof.get(i).getCantidad())>0)
                {
                    np.setMinValue(0);
                    np.setMaxValue(Integer.parseInt(sof.get(i).getCantidad()));
                    max=Integer.parseInt(sof.get(i).getCantidad());
                }
            }
        }
        //aqui min y max
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aler.dismiss();
            }
        });
        final int finalPos = pos;
        final int finalMax = max;
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int x = np.getValue();
                addon(nom, x);
                //lv.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                sof.get(finalPos).setCantidad(Integer.toString(finalMax - x));
                sumar();
                aler.dismiss();
            }
        });

        aler.setView(prom);
        aler.show();
    }
    public void sumar()
    {
        int te = 0;
        for (int i = 0; i < category.size(); i++) {
            te += Integer.parseInt(category.get(i).getPrecioTotal());
        }
        textView.setText(Integer.toString(te));
    }
    public void addon(String nombre, int cantidad)
    {
        //category.add(new DetalleDespacho(nombre, Integer.toString(cantidad), "0"));
        lv.setAdapter(adapter);
    }
    public void cargarKardeX()
    {
        SharedPreferences mPrefs = this.getSharedPreferences("LOL", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("ListaFinal", "");
        if (json.isEmpty()) {
            callLog = new ArrayList<Despacho>();
        } else {
            Type type = new TypeToken<ArrayList<Despacho>>() {
            }.getType();
            callLog = gson.fromJson(json, type);
        }
        sof.addAll(callLog.get(numero).detallesDespacho);
        nombre = callLog.get(numero).nombreCliente;
        observaciones = callLog.get(numero).observaciones;

    }

    public void guardarTodo()
    {

        verificar();
        saveSharedPreferencesLogList();
        Toast.makeText(ProductosLimite.this, "Correcto", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(ProductosLimite.this, Pedido.class)
                .putExtra("NOMBRE_K",nombre)
                .putExtra("OBS_K",observaciones)
                .putExtra("ACTIVITY","Limite")
                //el sof
        );
    }

    public void saveSharedPreferencesLogList()
    {


        Context context = this;
        SharedPreferences mPrefs = context.getSharedPreferences("LOL", context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(callLog);
        prefsEditor.putString("ListaFinal", json);
        prefsEditor.commit();


    }


    public void verificar()
    {
        //callLog.get(numero).setList(sof);
        int x=0;
        for(int i=0; i<sof.size(); i++)
        {
            if(Integer.parseInt(sof.get(i).getCantidad())==0)
            {
                x++;
            }
        }
        if(x==sof.size())
        {
            callLog.remove(numero);
        }
        else{callLog.get(numero).setDetalleDespacho(sof);}
    }


}
