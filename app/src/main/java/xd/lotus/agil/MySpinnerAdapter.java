package xd.lotus.agil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import models.Destino;

//custom adapter
public class MySpinnerAdapter extends ArrayAdapter<Destino> {

    private Destino[] myObjs;

    public MySpinnerAdapter(Context context, int textViewResourceId,
                            ArrayList<Destino> myObjs) {
        super(context, textViewResourceId, myObjs);
        this.myObjs = myObjs.toArray(new Destino[0]);
    }

    public int getCount(){
        return myObjs.length;
    }

    public Destino getItem(int position){
        return myObjs[position];
    }

    public long getItemId(int position){
        return position;
    }



    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View dropDownView = inflater.inflate(R.layout.custom_spinner, parent, false);

        TextView dd_GoText = (TextView)dropDownView.findViewById(R.id.gotext);
        dd_GoText.setText(myObjs[position].toString());

        TextView dd_GoAddr = (TextView)dropDownView.findViewById(R.id.goaddr);
        dd_GoAddr.setText(myObjs[position].getDireccion());

        return dropDownView;

    }

}