package xd.lotus.agil;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import models.Category;

public class Mapas extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapas);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                //buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            } else {
                //Request Location Permission
                //checkLocationPermission();
            }
        }
        else {
            //buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }

        final Marker celeste = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(-17.382599, -66.154053))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN))
                .title("Ferreteria Cuper"));
        celeste.setTag(0);

        Marker azul = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(-17.382517, -66.159750))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                .title("Ferreteria Amancayas"));
        azul.setTag(0);

        Marker verde = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(-17.376275, -66.159056))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                .title("Ferreteria Super hombre"));
        verde.setTag(0);

        CameraPosition cameraPosition = new CameraPosition.Builder().target(
                new LatLng(-17.382599, -66.154053)).zoom(14).build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if(marker.equals(celeste))
                {
                    Toast.makeText(Mapas.this, "Ha sido presionado el boton celeste >:v", Toast.LENGTH_SHORT).show();
                    mostrarPop();
                }
                return false;
            }
        });
    }
    public void mostrarPop()
    {
            LayoutInflater inf = LayoutInflater.from(this);
            //inflamos con un layout personalizado
            View prom = inf.inflate(R.layout.folo,null);
            final AlertDialog aler = new AlertDialog.Builder(this).create();

            TextView nombreFolo = prom.findViewById(R.id.nomFolo);
            TextView fechaFOlo = prom.findViewById(R.id.fechaFolo);
            TextView sumaTotalT = prom.findViewById(R.id.sumaTotal);
            //aler.setTitle("Detalles");
            aler.setCanceledOnTouchOutside(false);

            aler.setButton(DialogInterface.BUTTON_POSITIVE, "Salir", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    aler.dismiss();
                    //nel pastel
                }
            });
            aler.setButton(DialogInterface.BUTTON_NEGATIVE, "Guardar PDF", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //guardar pdf
                }
            });

            aler.setView(prom);
            aler.show();

    }



}
