package xd.lotus.agil;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import models.Cliente;
import models.ClienteRazon;
import models.Despacho;
import models.Destino;
import utils.Functions;

public class Pedido extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    JSONObject jsonObject;
    JSONArray jsonArray;
    ArrayList<Cliente> clientes;
    ArrayList<ClienteRazon> clientesRazon;
    ArrayList<Destino> destinos;
    Despacho despacho;
    String id_empresa,id_usuario;

    private Location mylocation;
    private GoogleApiClient googleApiClient;
    private final static int REQUEST_CHECK_SETTINGS_GPS=0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS=0x2;

short geof =0;
private int mYear, mMonth, mDay;
    EditText fecha;
    ArrayList<String>facturar;
    ArrayList<Integer>nit;
    ArrayList<String>destino;
    ArrayList<String>direccion;
    Spinner spinnerFactura;
    ArrayAdapter<String>facturaAdapter;
    Spinner spinnerDestino;
    ArrayAdapter<String>destinoAdapter;
    TextView textDireccion;
    ArrayAdapter<String>direccionAdapter;
    TextView nitView;
    AutoCompleteTextView nombreCLiente;
    TextView obs;
    String guardadoLocal="";
    String guardadoFactura="";
    int numero;
    String limite="";
    Button addFacturar;
    Button addDestino;
    LinearLayout facturarPedido;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedido);
        getSupportActionBar().setTitle("PEDIDO");

        id_empresa= getIntent().getStringExtra("ID_EMPRESA");
        id_usuario= getIntent().getStringExtra("ID_USUARIO");
        guardadoLocal= getIntent().getStringExtra("GUARDADO_LOCAL");
        guardadoFactura= getIntent().getStringExtra("GUARDADO_FACTURA");
        numero= getIntent().getIntExtra("NUMEROX",0);

        clientes=new ArrayList<>();

        clientesRazon=new ArrayList<>();
        destinos=new ArrayList<>();

        limite = getIntent().getStringExtra("ACTIVITY");

        final Button geo = findViewById(R.id.habilitarGeo);
        geo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if(geof==0) {
                geo.setBackgroundResource(R.drawable.nogeo);geof=1;
            }
            else{
                geo.setBackgroundResource(R.drawable.geo); geof=0;
                nombreCLiente.setError(null);
                //calcular distancia menor
                new LecturaTodosClientesEmpresa().execute(Functions.REST_URL +"clientes/empresa/"+id_empresa);

                //Toast.makeText(Pedido.this,"latitude "+ mylocation.getLatitude(), Toast.LENGTH_SHORT).show();
                //Toast.makeText(Pedido.this,"longitude "+ mylocation.getLongitude(), Toast.LENGTH_SHORT).show();
            }
            }
        });
        setUpGClient();

        fecha = findViewById(R.id.fecha);

        obs = findViewById(R.id.obs);

        obtenerFecha();

        Button fechaC = findViewById(R.id.fechaC);
        fechaC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker();
            }
        });

        facturar= new ArrayList<>();
        nit=new ArrayList<>();
        nitView = findViewById(R.id.nitView);
        destino = new ArrayList<>();
        direccion = new ArrayList<>();

        obs.setText(getIntent().getStringExtra("OBS_K"));

        llenar();

        addDestino = findViewById(R.id.addDestino);
        addDestino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDestinos();
            }
        });
        addDestino.setEnabled(false);

        Button ok = findViewById(R.id.okb);
        Button nook = findViewById(R.id.nook);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pasarInventario();
            }
        });

        nook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Pedido.this, "Cancelado", Toast.LENGTH_SHORT).show();
                Intent i=new Intent(Pedido.this, Select.class);
                i.putExtra("ID_EMPRESA",id_empresa);
                i.putExtra("ID_USUARIO",id_usuario);
                startActivity(i);
            }
        });

         addFacturar = findViewById(R.id.addFacturar);
        addFacturar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ingresarFacturar();
                addPersona();
            }
        });
        addFacturar.setEnabled(false);

        nombreCLiente = (AutoCompleteTextView)findViewById(R.id.nombreCliente);
        spinnerDestino = findViewById(R.id.spinnerDestino);
        textDireccion = findViewById(R.id.textDireccion);
        spinnerFactura = findViewById(R.id.spinnerFactura);
        facturarPedido = findViewById(R.id.facturarPedido);

        if (guardadoFactura.equals("1")){
            facturarPedido.setVisibility(View.GONE);
            nitView.setVisibility(View.GONE);
        }


        if(guardadoLocal.equals("1")){
            despacho=(models.Despacho)getIntent().getSerializableExtra("despacho");
            establecerCliente(null,0,despacho.idCliente);
            nombreCLiente.setText(despacho.nombreCliente);
            nombreCLiente.setEnabled(false);
            geo.setEnabled(false);
            obs.setText(despacho.observaciones);
        }else{
            despacho=new Despacho();
        }

        nombreCLiente.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View arg0, int arg1, KeyEvent arg2) {
                // TODO Auto-generated method stub
                if ((arg2.getAction() == KeyEvent.ACTION_DOWN) &&
                        (arg1 == KeyEvent.KEYCODE_ENTER)) {
                    String text=nombreCLiente.getText().toString().replace(" ","%20");

                    new LecturaClientesEmpresa().execute(Functions.REST_URL +"clientes/empresa/"+id_empresa+"/texto/"+text);
                }

                // return true; - if consumed
                return false;
            }
        });

        nombreCLiente.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                establecerCliente(parent,position,0);
            }
        });


        spinnerFactura.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ClienteRazon item = (ClienteRazon)parent.getItemAtPosition(position);
                nitView.setText(item.getNit()+"");
                despacho.idClienteRazon=item.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinnerDestino.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Destino item = (Destino)parent.getItemAtPosition(position);
                textDireccion.setText(item.getDireccion()+"");
                despacho.idDestino=item.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private synchronized void setUpGClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        mylocation = location;
        if (mylocation != null) {
            Double latitude=mylocation.getLatitude();
            Double longitude=mylocation.getLongitude();

        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        checkPermissions();
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Do whatever you need
        //You can display a message here
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //You can display a message here
    }

    private void checkPermissions(){
        int permissionLocation = ContextCompat.checkSelfPermission(Pedido.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this,
                        listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            }
        }else{
            getMyLocation();
        }

    }

    private void getMyLocation(){
        if(googleApiClient!=null) {
            if (googleApiClient.isConnected()) {
                int permissionLocation = ContextCompat.checkSelfPermission(Pedido.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    mylocation =                     LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(3000);
                    locationRequest.setFastestInterval(3000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi
                            .requestLocationUpdates(googleApiClient, locationRequest, this);
                    PendingResult<LocationSettingsResult> result =
                            LocationServices.SettingsApi
                                    .checkLocationSettings(googleApiClient, builder.build());
                    result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

                        @Override
                        public void onResult(LocationSettingsResult result) {
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    // All location settings are satisfied.
                                    // You can initialize location requests here.
                                    int permissionLocation = ContextCompat
                                            .checkSelfPermission(Pedido.this,
                                                    Manifest.permission.ACCESS_FINE_LOCATION);
                                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                                        mylocation = LocationServices.FusedLocationApi
                                                .getLastLocation(googleApiClient);
                                    }
                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.
                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        status.startResolutionForResult(Pedido.this,
                                                REQUEST_CHECK_SETTINGS_GPS);
                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied.
                                    // However, we have no way
                                    // to fix the
                                    // settings so we won't show the dialog.
                                    // finish();
                                    break;
                            }
                        }
                    });
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        int permissionLocation = ContextCompat.checkSelfPermission(Pedido.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            getMyLocation();
        }
    }

    public void establecerCliente(AdapterView<?> parent,int position,int idCliente){
        if(idCliente==0){
            Cliente item = (Cliente)parent.getItemAtPosition(position);
            despacho.idCliente=item.getId();
            new LecturaCliente().execute(Functions.REST_URL +"clientes/"+item.getId());
        }else{
            despacho.idCliente=idCliente;
            new LecturaCliente().execute(Functions.REST_URL +"clientes/"+idCliente);
        }
        addFacturar.setEnabled(true);
        addDestino.setEnabled(true);
    }

    private class LecturaTodosClientesEmpresa extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            return Functions.readJSONFeed(urls[0]);
        }

        protected void onPostExecute(String result) {
            try {
                // Locate the NodeList name
                jsonArray = new JSONArray(result);
                clientes=new ArrayList<>();
                Cliente clienteAproximado=new Cliente();
                clienteAproximado.distancia=9999999;
                for (int i = 0; i < jsonArray.length(); i++) {
                    jsonObject = jsonArray.getJSONObject(i);
                    Cliente cliente = new Cliente(jsonObject.getInt("id"),jsonObject.getString("razon_social"),jsonObject.getLong("nit"),jsonObject.getDouble("latitud"),jsonObject.getDouble("longitud"));
                    clientes.add(cliente);
                    Location locationCliente = new Location("");
                    locationCliente.setLatitude(cliente.latitud);
                    locationCliente.setLongitude(cliente.longitud);
                    double actualLatitud=mylocation.getLatitude();
                    double actualLongitud=mylocation.getLongitude();
                    double distancia=locationCliente.distanceTo(mylocation);
                    clienteAproximado=obtenerMenorDistancia(clienteAproximado.distancia,clienteAproximado,distancia,cliente);
                }
                establecerCliente(null,0,clienteAproximado.getId());
                nombreCLiente.setText(clienteAproximado.toString());

            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
        }

        public Cliente obtenerMenorDistancia(double distancia1,Cliente c1,double distancia2,Cliente c2){
            Cliente res;
            if(distancia1<distancia2){
                res=c1;
                res.distancia=distancia1;
            }else{
                res=c2;
                res.distancia=distancia2;
            }
            return res;
        }
    }

    private class LecturaClientesEmpresa extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            return Functions.readJSONFeed(urls[0]);
        }

        protected void onPostExecute(String result) {
            try {
                Log.e("el datoooooooooo", result);
                jsonArray = new JSONArray(result);
                if (jsonArray.length()>0) {
                    // Locate the NodeList name

                    clientes = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);
                        Cliente cliente = new Cliente(jsonObject.getInt("id"), jsonObject.getString("razon_social"), jsonObject.getLong("nit"));
                        clientes.add(cliente);
                        /*Spinner ciudad = findViewById(R.id.spinner2);
                        ArrayAdapter adapter=new ArrayAdapter<String>(REG1.this,
                                android.R.layout.simple_spinner_item,
                                nombresDepartamentos);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        ciudad
                                .setAdapter(adapter);*/

                        ArrayAdapter<Cliente> adapter =
                                new ArrayAdapter<Cliente>(Pedido.this, android.R.layout.simple_list_item_1, clientes);
                        nombreCLiente.setAdapter(adapter);
                        nombreCLiente.showDropDown();

                    }
                }else{

                    Log.e("el dato escrito ", String.valueOf(nombreCLiente.getText()));
                    addCliente(String.valueOf(nombreCLiente.getText()));


                }
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public void addCliente(String nombre)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater layoutInflater = this.getLayoutInflater();
        final View dialog = layoutInflater.inflate(R.layout.cliente, null);
        builder.setView(dialog);
        final AlertDialog alertDialog = builder.create();

        //cosas
        final EditText addNombres = dialog.findViewById(R.id.nombreClienteR);
        addNombres.setText(nombre);
        final EditText addTelefono = dialog.findViewById(R.id.telefonoCliente);
        final EditText addContacto = dialog.findViewById(R.id.contactoCliente);
        Button cancelarAdd = dialog.findViewById(R.id.cancelarAdd);
        Button aceptarAdd = dialog.findViewById(R.id.aceptarAdd);

        cancelarAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        aceptarAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(addNombres.getText().toString().length() == 0)
                {
                    addNombres.setError("ingrese el nombre");
                    addNombres.requestFocus();
                }
                else {
                    new GuardarClienteNuevo().execute(addNombres.getText().toString(), addTelefono.getText().toString(), addContacto.getText().toString());
                    nombreCLiente.setText(addNombres.getText().toString());
                    addFacturar.setEnabled(true);
                    addDestino.setEnabled(true);
                    Toast.makeText(Pedido.this, "El cliente se registro", Toast.LENGTH_SHORT).show();
                    alertDialog.dismiss();
                }
            }
        });
        alertDialog.show();
    }

    private class GuardarClienteNuevo extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... data) {
            JSONObject datos = new JSONObject();
            try {
                datos.put("razon_social",data[0]);
                datos.put("telefono",data[1]);
                datos.put("contacto",data[2]);

                JSONObject res = Functions.postJSONdata("clientes-pedido/"+id_empresa,datos);

//                String idCliente = String.valueOf(resultado.getJSONObject("id"));
                Log.e("el resultadooo", String.valueOf(res));
                String idCliente = res.getString("id");
                Log.e("el ideeeeeeeeee", String.valueOf(idCliente));
                despacho.idCliente=Integer.valueOf(idCliente);


                establecerCliente(null,0,Integer.valueOf(idCliente));
            } catch (JSONException ex) {
                //Logger.getLogger(EditPeymentConcept.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "";
        }
    }

    private class LecturaCliente extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            return Functions.readJSONFeed(urls[0]);
        }

        protected void onPostExecute(String result) {
            try {
                // Locate the NodeList name
                jsonObject = new JSONObject(result);
                JSONArray clientesDestinoJson = jsonObject.getJSONArray("cliente_destinos");
                destinos=new ArrayList<>();
                if (clientesDestinoJson.length()>0) {
                    for (int i = 0; i < clientesDestinoJson.length(); i++) {
                        JSONObject clienteDestinoJson = clientesDestinoJson.getJSONObject(i);
                        JSONObject destinoJson = clienteDestinoJson.getJSONObject("destino");
                        Destino destino = new Destino(destinoJson.getInt("id"), destinoJson.getString("destino"), destinoJson.getString("direccion"));

                        destinos.add(destino);

                        //                    ArrayAdapter<Destino> adapter=new ArrayAdapter<Destino>(Pedido.this,
                        //                            android.R.layout.simple_spinner_item,
                        //                            destinos);
                        //                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        MySpinnerAdapter adapter =
                                new MySpinnerAdapter(Pedido.this,
                                        android.R.layout.simple_spinner_item,
                                        destinos);

                        spinnerDestino.setAdapter(adapter);

                    }
                }

                clientesRazon=new ArrayList<>();
                if (jsonObject.getJSONArray("clientes_razon").length() > 0) {
                    for (int i = 0; i < jsonObject.getJSONArray("clientes_razon").length(); i++) {
                        JSONObject clienteRazonJson = jsonObject.getJSONArray("clientes_razon").getJSONObject(i);

                        ClienteRazon clienteRazon = new ClienteRazon(clienteRazonJson.getInt("id"), clienteRazonJson.getString("razon_social"), clienteRazonJson.getLong("nit"));

                        clientesRazon.add(clienteRazon);

                        /*Spinner ciudad = findViewById(R.id.spinner2);
                        ArrayAdapter adapter=new ArrayAdapter<String>(REG1.this,
                                android.R.layout.simple_spinner_item,
                                nombresDepartamentos);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        ciudad
                                .setAdapter(adapter);*/
                        ArrayAdapter<ClienteRazon> adapter = new ArrayAdapter<ClienteRazon>(Pedido.this,
                                android.R.layout.simple_spinner_item,
                                clientesRazon);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinnerFactura.setAdapter(adapter);

                    }
                }
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
        }
    }

//    //custom adapter
//    public class MySpinnerAdapter extends ArrayAdapter<Destino>{
//
//        private Destino[] myObjs;
//
//        public MySpinnerAdapter(Context context, int textViewResourceId,
//                                ArrayList<Destino> myObjs) {
//            super(context, textViewResourceId, myObjs);
//            this.myObjs = myObjs.toArray(new Destino[0]);
//        }
//
//        public int getCount(){
//            return myObjs.length;
//        }
//
//        public Destino getItem(int position){
//            return myObjs[position];
//        }
//
//        public long getItemId(int position){
//            return position;
//        }
//
//
//
//        @Override
//        public View getDropDownView(int position, View convertView,
//                                    ViewGroup parent) {
//            LayoutInflater inflater = getLayoutInflater();
//            View dropDownView = inflater.inflate(R.layout.custom_spinner, parent, false);
//
//            TextView dd_GoText = (TextView)dropDownView.findViewById(R.id.gotext);
//            dd_GoText.setText(myObjs[position].toString());
//
//            TextView dd_GoAddr = (TextView)dropDownView.findViewById(R.id.goaddr);
//            dd_GoAddr.setText(myObjs[position].getDireccion());
//
//            return dropDownView;
//
//        }
//
//    }

    public void obtenerFecha()
    {

        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 1);
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd/MM/yyyy");

        fecha.setText(simpleDateFormat1.format(c.getTime()));
//        fecha.setText(Integer.toString(mDay)+"/"+Integer.toString(mMonth)+"/"+mYear);
    }

    public void darFecha(int x, int y, int z)
    {
        mYear = x;
        mMonth = y;
        mDay = z;
        fecha.setText(Integer.toString(mDay)+"/"+Integer.toString(mMonth)+"/"+mYear);
    }

    public void picker()
    {
        final DatePickerDialog dl = new DatePickerDialog(this, null, mYear,mMonth-1,mDay);
        dl.setButton(DialogInterface.BUTTON_POSITIVE, "Guardar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                darFecha(dl.getDatePicker().getYear(),dl.getDatePicker().getMonth()+1,dl.getDatePicker().getDayOfMonth());
                dl.dismiss();
            }
        });
        dl.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dl.dismiss();
            }
        });
        dl.show();
    }

    public void addPersona()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater layoutInflater = this.getLayoutInflater();
        final View dialog = layoutInflater.inflate(R.layout.fechapicker, null);
        builder.setView(dialog);
        final AlertDialog alertDialog = builder.create();

        //cosas
        final EditText addNombres = dialog.findViewById(R.id.addNombres);
        final EditText addNits = dialog.findViewById(R.id.addNits);
        Button cancelarAdd = dialog.findViewById(R.id.cancelarAdd);
        Button aceptarAdd = dialog.findViewById(R.id.aceptarAdd);

        cancelarAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        aceptarAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(addNombres.getText().toString().length() == 0)
                {
                    addNombres.setError("ingrese el nombre");
                    addNombres.requestFocus();
                }
                else if(addNits.getText().toString().length()== 0)
                {
                    addNits.setError("ingrese el nit");
                    addNits.requestFocus();
                }
                else {
                    new GuardarClienteRazon().execute(addNombres.getText().toString(), addNits.getText().toString());
                    alertDialog.dismiss();
                }
            }
        });
        alertDialog.show();
    }
    public void addDestinos()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater layoutInflater = this.getLayoutInflater();
        final View dialog = layoutInflater.inflate(R.layout.destinopicker, null);
        builder.setView(dialog);
        final AlertDialog alertDialog = builder.create();

        //cosas
        final EditText addDestinos = dialog.findViewById(R.id.addDestinoX);
        final EditText addDireccion = dialog.findViewById(R.id.addDireccionXf);
        Button cancelarDestino = dialog.findViewById(R.id.cancelarDestino);
        Button aceptarDestino = dialog.findViewById(R.id.aceptarDestino);


        cancelarDestino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        aceptarDestino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(addDestinos.getText().toString().length() == 0)
                {
                    addDestinos.setError("ingrese el destino");
                    addDestinos.requestFocus();
                }
                else if(addDireccion.getText().toString().length()== 0)
                {
                    addDireccion.setError("ingrese la direccion");
                    addDireccion.requestFocus();
                }
                else {
                    new GuardarClienteDestino().execute(addDestinos.getText().toString(), addDireccion.getText().toString());
                    Log.e("llego", "llego a elseeee");
                    alertDialog.dismiss();
                }
            }
        });
        alertDialog.show();

    }

    public void llenar()
    {
        facturar.add("David Castellón");
        facturar.add("Carlos Quiroga");
        facturar.add("Tomás Aquino");
        nit.add(45454545);
        nit.add(12121212);
        nit.add(32323232);
        destino.add("Ferretería Cuper");
        destino.add("Ferretería Amancayas");
        destino.add("Ferretería Los Andes");
        direccion.add("Av. Villazón, entre Ayacucho y Monseñor Roberto Nicoli");
        direccion.add("Prado, frente al edificio de Comteco");
        direccion.add("Plazuela Quintanilla, frente al edificio de Los Tiempos");
    }

    public void pasarInventario()
    {

        despacho.fecha=fecha.getText().toString();
        TextView errorTextFactura = (TextView)spinnerFactura.getSelectedView();
        TextView errorTextDestino = (TextView)spinnerDestino.getSelectedView();
//        View selectedView = spinnerFactura.getSelectedView();
//        TextView selectedTextView = (TextView) selectedView;

        if(nombreCLiente.getText().toString().length() == 0)
        {
            nombreCLiente.setError("ingrese el cliente");
            nombreCLiente.requestFocus();
        }

        else if(guardadoFactura.equals("0") && errorTextFactura == null){
            TextView tvInvisibleError = findViewById(R.id.tvFacturaError);
            tvInvisibleError.requestFocus();
            tvInvisibleError.setError("Agregue una factura");

//            errorTextFactura.setTextColor(Color.RED);//just to highlight that this is an error
//            errorTextFactura.setText("Agregue una factura");

        }
//        else if(errorTextFactura.getText().equals("") || errorTextFactura.getText().equals("Agregue una factura")){
//            errorTextFactura.setError("");
//            errorTextFactura.setTextColor(Color.RED);//just to highlight that this is an error
//            errorTextFactura.setText("Agregue una factura");
//        }

        else if(errorTextDestino == null){
            TextView tvInvisibleErrorD = findViewById(R.id.tvDestinoError);
            tvInvisibleErrorD.requestFocus();
            tvInvisibleErrorD.setError("Agregue un destino");
        }

        else if(guardadoFactura.equals("0") && nitView.getText().toString().length() == 0){
            nitView.setError("ingrese el nit");
            nitView.requestFocus();
        }

        else if(textDireccion.getText().toString().length() == 0){
            textDireccion.setError("ingrese la direccion");
            textDireccion.requestFocus();
        }

        else {

            if (guardadoLocal.equals("1")) {
                despacho.observaciones = obs.getText().toString();
                guardarTodo();
            } else {
//                para enviar a detalle inventario cuando es pedido normal sin kardex
                despacho.observaciones = obs.getText().toString();
                Intent i = new Intent(Pedido.this, Inventario.class);
                i.putExtra("KEY_CLIENTE", nombreCLiente.getText().toString());
                i.putExtra("KEY_DESPACHO", fecha.getText().toString());
                i.putExtra("KEY_FACTURAR", spinnerFactura.getSelectedItem().toString());
                i.putExtra("KEY_NIT", nitView.getText().toString());
                i.putExtra("KEY_DESTINO", spinnerDestino.getSelectedItem().toString());
                i.putExtra("KEY_DIRECCION", textDireccion.getText().toString());
                i.putExtra("KEY_OBS", obs.getText().toString());
                i.putExtra("ACTIVITY", limite);
                i.putExtra("despacho", despacho);
                i.putExtra("ID_EMPRESA", id_empresa);
                i.putExtra("ID_USUARIO", id_usuario);
                if (mylocation != null) {
                    i.putExtra("latitud", mylocation.getLatitude());
                    i.putExtra("longitud", mylocation.getLongitude());
                }else{
                    i.putExtra("latitud", 0);
                    i.putExtra("longitud", 0);
                }

                startActivity(i);
            }
        }
    }

    public void guardarTodo()
    {
//        obtengo los datos de la base datos con 'numero' y actualizar los datos de los detalles ===
//        mandar los detalles  con los datos a actualizar ======
//        ArrayList<Despacho> callLog;
//        SharedPreferences mPrefs = this.getSharedPreferences("LOL", Context.MODE_PRIVATE);
//        SharedPreferences.Editor prefsEditor = mPrefs.edit();
//        Gson gson = new Gson();
//        String json = mPrefs.getString("ListaFinal", "");
//
//        Log.e("los datos local ", json);
//
//        if (!json.isEmpty()) {
//            Type type = new TypeToken<ArrayList<Despacho>>() {
//            }.getType();
//            callLog = gson.fromJson(json, type);
//            Double sumaCantidad = 0.0;
////            callLog.get(numero).detallesDespacho.clear();
////            callLog.get(numero).setDetalleDespacho(despacho.detallesDespacho);
//            for(int i=0;i<despacho.detallesDespacho.size();i++){
////                al agregar nuevo producto nuevo debe almacenar en el local del celular
//
////                Integer d = i +1;
////                if (callLog.get(numero).detallesDespacho.size()-d < 0){
////                    Log.e("hay uno nulooooooo", "holaaaaaaaa");
////                    callLog.get(numero).detallesDespacho.add(i, despacho.detallesDespacho.get(i));
////
////                }
//                Double  cantidaLoc = Double.parseDouble(callLog.get(numero).detallesDespacho.get(i).cantidad) - Double.parseDouble(despacho.detallesDespacho.get(i).cantidad);
//                if (cantidaLoc > 0){
//                    callLog.get(numero).detallesDespacho.get(i).setCantidad(String.valueOf(cantidaLoc));
//                }else{
//                    callLog.get(numero).detallesDespacho.remove(i);
//                }
//
//                sumaCantidad = sumaCantidad + cantidaLoc;
//
//            }
//
//            if (sumaCantidad <= 0){
//                callLog.remove(numero);
//            }
//
//            gson = new Gson();
//
//            json = gson.toJson(callLog);
//            prefsEditor.putString("ListaFinal", json);
//            prefsEditor.commit();
//        }
        if (guardadoFactura.equals("1")){
            new ActualizarKardex().execute(Functions.REST_URL +"gtm-despacho-kardex-factura/empresa/"+id_empresa+"/usuario/"+id_usuario+"/factura/"+true);
        }else{
            new ActualizarKardex().execute(Functions.REST_URL +"gtm-despacho-kardex-factura/empresa/"+id_empresa+"/usuario/"+id_usuario+"/factura/"+false);
        }

//        new GuardarDespacho().execute();
        Toast.makeText(Pedido.this, "Pedido exitoso", Toast.LENGTH_SHORT).show();
        Intent i=new Intent(Pedido.this, Select.class);
        i.putExtra("ID_EMPRESA",id_empresa);
        i.putExtra("ID_USUARIO",id_usuario);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    private class ActualizarKardex extends AsyncTask<String, Void, String> {

        protected String doInBackground(String... urls) {
            return Functions.readJSONFeed(urls[0]);
        }

        protected void onPostExecute(String result) {
            JSONObject datosDespacho = new JSONObject();
            try {
                datosDespacho.put("id_cliente",despacho.idCliente);
                datosDespacho.put("id_destino",despacho.idDestino);
                datosDespacho.put("id_cliente_razon",despacho.idClienteRazon);
                datosDespacho.put("fecha",despacho.fecha);
                datosDespacho.put("observacion",despacho.observaciones);
                datosDespacho.put("id_usuario",despacho.id_usuario);
                JSONObject datos = new JSONObject();
                datos.put("id_cliente",despacho.idCliente);
                datos.put("id_destino",despacho.idDestino);
                datos.put("id_cliente_razon",despacho.idClienteRazon);
                datos.put("fecha",despacho.fecha);
                datos.put("observacion",despacho.observaciones);
                datos.put("id_usuario",despacho.id_usuario);
                JSONArray detallesDespacho=new JSONArray();


//                descomponer el resultado y mandarlo al array modelo de pendientes =======================
//                =========================================================================================
                jsonArray = new JSONArray(result);

                JSONObject dato = jsonArray.getJSONObject(numero);
                JSONArray kardex = dato.getJSONArray("detalles_kardex");
                JSONArray detallesKardex=new JSONArray();
                Log.e("actualizarrr", String.valueOf(kardex));

//                para la fecha ===
                int dia, mes, ano;
                Calendar calendar = Calendar.getInstance();
                dia = calendar.get(Calendar.DAY_OF_MONTH);
                mes= calendar.get(Calendar.MONTH);
                ano= calendar.get(Calendar.YEAR);
                String fechaActual = Integer.toString(dia)
                        +"/"+Integer.toString(mes)+"/"
                        +Integer.toString(ano);

//                corregir no sta llegando al post y modificar la variable "detallesDespacho" ===============
                for (int i = 0; i < kardex.length(); i++) {

                    jsonObject = kardex.getJSONObject(i);
                    JSONObject producto = jsonObject.getJSONObject("producto");
                    JSONObject objProducto = new JSONObject();

                    for(int j=0;j<despacho.detallesDespacho.size();j++){
                        if(j==i){
                            double cantidad = Double.parseDouble(despacho.detallesDespacho.get(j).cantidad);
                            double saldo = jsonObject.getDouble("saldo")-cantidad;
                            objProducto.put("id", jsonObject.getInt("id"));
                            objProducto.put("fecha", fechaActual);
                            objProducto.put("id_producto", jsonObject.getInt("id_producto"));
                            objProducto.put("saldo", saldo);
                            objProducto.put("cantidad_despacho", cantidad);
                            objProducto.put("entregado", false);
                            objProducto.put("id_kardex", jsonObject.getInt("id_kardex"));
                            objProducto.put("cantidad",  jsonObject.getDouble("cantidad"));
                            objProducto.put("cantidad_pedido",  despacho.detallesDespacho.get(i).cantidad);
                            objProducto.put("precio_unitario",despacho.detallesDespacho.get(i).getPrecioUnitario());
                            objProducto.put("total",despacho.detallesDespacho.get(i).precioTotal);

                            if (mylocation != null) {
                                objProducto.put("latitud", mylocation.getLatitude());
                                objProducto.put("longitud", mylocation.getLongitude());
                            }else {
                                objProducto.put("latitud", 0);
                                objProducto.put("longitud", 0);
                            }
                            detallesKardex.put(objProducto);
                        }else{
                            Log.e("entro al otro: ", "no");
                        }
                    }
                }

                datos.put("detalles_kardex", detallesKardex);
//                    Functions.postJSONdata("pedido-kardex/gtm-despacho/empresa/" + id_empresa, datos);
                GuardarDespachoKardex guardar = new GuardarDespachoKardex();
                guardar.datos = datos;
                guardar.execute();

            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
        }

    }

    private class GuardarDespachoRuta extends AsyncTask<Object, Void, Object> {
        protected JSONObject datos;

        public GuardarDespachoRuta(){
            this.datos = datos;
        }

        @Override
        protected String doInBackground(Object... objects) {

            Functions.postJSONdata("gtm-despacho/empresa/"+id_empresa, datos);

            return "";
        }
    }

    private class GuardarDespachoKardex extends AsyncTask<Object, Void, Object> {
        protected JSONObject datos;

        public GuardarDespachoKardex(){
            this.datos = datos;
        }

        @Override
        protected String doInBackground(Object... objects) {

            Functions.postJSONdata("pedido-kardex/gtm-despacho/empresa/" + id_empresa, datos);

            return "";
        }
    }

    private class GuardarDespacho extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... data) {
            JSONObject datos = new JSONObject();
            try {
                datos.put("id_cliente",despacho.idCliente);
                datos.put("id_destino",despacho.idDestino);
                datos.put("id_cliente_razon",despacho.idClienteRazon);
                datos.put("fecha",despacho.fecha);
                datos.put("observacion",despacho.observaciones);
                datos.put("id_usuario",despacho.id_usuario);
                JSONArray detallesDespacho=new JSONArray();
                for(int i=0;i<despacho.detallesDespacho.size();i++){
                    JSONObject jsonobj=new JSONObject();
                    jsonobj.put("id_producto",despacho.detallesDespacho.get(i).id_producto);
                    jsonobj.put("cantidad",despacho.detallesDespacho.get(i).cantidad);
                    jsonobj.put("precio_unitario",despacho.detallesDespacho.get(i).getPrecioUnitario());
                    jsonobj.put("total",despacho.detallesDespacho.get(i).precioTotal);
//                    jsonobj.put("id_kardex", jsonObject.getInt("id_kardex"));
                    detallesDespacho.put(jsonobj);
                }
                datos.put("detalles_despacho",detallesDespacho);
                Functions.postJSONdata("gtm-despacho/empresa/"+id_empresa,datos);

            } catch (JSONException ex) {
                //Logger.getLogger(EditPeymentConcept.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "";
        }
    }

    private class GuardarClienteRazon extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... data) {
            JSONObject datos = new JSONObject();
            try {
                datos.put("razon_social",data[0]);
                datos.put("nit",data[1]);
                Functions.postJSONdata("clientes-razon/"+despacho.idCliente,datos);
                establecerCliente(null,0,despacho.idCliente);
            } catch (JSONException ex) {
                //Logger.getLogger(EditPeymentConcept.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "";
        }
    }

    private static final String TAG = "CreadorDePDF";

    private class GuardarClienteDestino extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... data) {
            JSONObject datos = new JSONObject();
            try {


                datos.put("id_empresa",id_empresa);
                datos.put("destino",data[0]);
                datos.put("direccion",data[1]);

                Functions.postJSONdata("clientes-destino/"+despacho.idCliente,datos);
                establecerCliente(null,0,despacho.idCliente);
            } catch (JSONException ex) {
                //Logger.getLogger(EditPeymentConcept.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "";
        }
    }


}
