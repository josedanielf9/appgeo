package xd.lotus.agil;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import models.DetalleDespacho;
import models.Producto;
import utils.Functions;

public class Productos extends AppCompatActivity {
ProductoAdapter mAdapter;
    String id_empresa,id_usuario;
ArrayList<Producto> productosList =new ArrayList<>();
    AdapterItem adapter;
    ArrayList<DetalleDespacho>category;
    //int[]precios={50,250,50,250,50,250,50,250};
    TextView textView ;
    ListView lv;
    models.Despacho despacho;


    String nombre;
    String observaciones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productos);

        getSupportActionBar().setTitle("Productos");

        despacho=(models.Despacho)getIntent().getSerializableExtra("despacho");
        id_empresa= getIntent().getStringExtra("ID_EMPRESA");
        id_usuario= getIntent().getStringExtra("ID_USUARIO");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        final RecyclerView recyclerView = findViewById(R.id.resor);
        recyclerView.setLayoutManager(linearLayoutManager);
        mAdapter = new ProductoAdapter(productosList);
        recyclerView.getLayoutManager().setMeasurementCacheEnabled(false);
        recyclerView.setAdapter(mAdapter);

        new LecturaProductos().execute(Functions.REST_URL +"productos/empresa/"+id_empresa+"/pagina/1/items-pagina/10/busqueda/0/columna/nombre/direccion/asc/grupo/0/user/"+id_usuario);


        textView = findViewById(R.id.textote);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new   RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // TODO Handle item click
                        //Toast.makeText(Productos.this, "XD "+position, Toast.LENGTH_SHORT ).show();
                        dialogador(productosList.get(position), position);
                    }
                })
        );

        category = new ArrayList<>();
        adapter = new AdapterItem(Productos.this, category);
        lv = findViewById(R.id.listaProductos);
        lv.setAdapter(adapter);

        Button aceptarTodo = findViewById(R.id.aceptarTodo);
        Button cancelarTodo = findViewById(R.id.cancelarTodo);

        aceptarTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarTodo();
            }
        });
        cancelarTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Productos.this, "Pedido cancelado", Toast.LENGTH_SHORT).show();
                Intent i=new Intent(Productos.this, Select.class);
                i.putExtra("ID_EMPRESA",id_empresa);
                i.putExtra("ID_USUARIO",id_usuario);
                startActivity(i);
            }
        });

    }

    private class LecturaProductos extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            return Functions.readJSONFeed(urls[0]);
        }

        protected void onPostExecute(String result) {
            try {
                // Locate the NodeList name
                JSONObject jsonObject = new JSONObject(result);
                JSONArray productosJson = jsonObject.getJSONArray("productos");
                for (int i = 0; i < productosJson.length(); i++) {
                    JSONObject productoJson = productosJson.getJSONObject(i);
                    Producto producto=new Producto(productoJson.getInt("id"),productoJson.getString("nombre"),productoJson.getString("imagen"),0,productoJson.getDouble("precio_unitario"));

                    productosList.add(producto);
                }
                mAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public void dialogador(final Producto producto, final int pos)
    {
        final String nom=producto.getNombre();
        //inflater
        LayoutInflater inf = LayoutInflater.from(this);
        //inflamos con un layout personalizado
        View prom = inf.inflate(R.layout.roke,null);
        final AlertDialog aler = new AlertDialog.Builder(this).create();

        aler.setTitle("Producto");

//        TextView txtNom = prom.findViewById(R.id.nombreProducto);
        final EditText edCant = prom.findViewById(R.id.cantidadProducto);
        final EditText edPrec = prom.findViewById(R.id.precioFinalProducto);
        final EditText trans = prom.findViewById(R.id.transporte);
        final EditText totaltrans = prom.findViewById(R.id.totaltransporte);
        totaltrans.setEnabled(false);

        trans.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String dtrans = "0";
                if (s.length() > 0){
                    dtrans = s.toString();
                }
                double caltransporte = Double.parseDouble(edCant.getText().toString()) * Double.parseDouble(dtrans);

                totaltrans.setText(String.valueOf(caltransporte));
            }
        });

        Button canCant = prom.findViewById(R.id.canCantidad);
        Button acepCantidad = prom.findViewById(R.id.acepCantidad);

        edCant.append("1");
        trans.append("0");
        edPrec.setText(producto.getPrecio().toString());

        edCant.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String dcant = "0";
                if (s.length() > 0){
                    dcant = s.toString();
                }
                double caltransporte2 = Double.parseDouble(dcant) * Double.parseDouble(trans.getText().toString());

                totaltrans.setText(String.valueOf(caltransporte2));
            }
        });
//        txtNom.setText(nom);
        canCant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aler.dismiss();
            }
        });
        acepCantidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edCant.length()==0)
                {
                    aler.dismiss();
                }
                else {
                double total = Double.parseDouble(edPrec.getText().toString())*Double.parseDouble(edCant.getText().toString());
                int cantidad = Integer.parseInt(edCant.getText().toString());
                category.add(new DetalleDespacho(producto.getId(),nom, Integer.toString(cantidad), edPrec.getText().toString(), Double.toString(total), trans.getText().toString()));
                adapter.notifyDataSetChanged();
                sumar();
                aler.dismiss();

                }
            }
        });

        aler.setView(prom);
        aler.show();
    }
    public void sumar()
    {
        double te= 0;
        for(int i = 0; i<category.size(); i++)
        {
            te+=Double.parseDouble(category.get(i).getPrecioTotal());
        }
        textView.setText(Double.toString(te));
    }



    public void eliminarX(View v)
    {
        final int position = lv.getPositionForView((View)v.getParent());
        restar(position);
        category.remove(position);
        lv.setAdapter(adapter);
        //adapter.notifyDataSetChanged();

    }
    public void restar(int p)
    {
        double total = Double.parseDouble(textView.getText().toString());
        double sus = Double.parseDouble(category.get(p).getPrecioTotal());
        total= total-sus;
        textView.setText(Double.toString(total));
    }
    public void editarX(View v)
    {
        final int posi = lv.getPositionForView((View)v.getParent());
        restar(posi);
        editor(posi);
    }
    /*public void guardarEditado(String nombre, double cantidad, double precio, int pos)
    {
        category.set(pos, new DetalleDespacho(nombre, Double.toString(cantidad), Double.toString(precio)));
        adapter.notifyDataSetChanged();
        sumar();
    }*/

    public void editor(final int eje)
    {
        //inflater
        LayoutInflater inf = LayoutInflater.from(this);
        //inflamos con un layout personalizado
        View prom = inf.inflate(R.layout.roke,null);
        final AlertDialog aler = new AlertDialog.Builder(this).create();

        aler.setTitle("Producto");

//        TextView txtNom = prom.findViewById(R.id.nombreProducto);
        final EditText edCant = prom.findViewById(R.id.cantidadProducto);
        final EditText edPrec = prom.findViewById(R.id.precioFinalProducto);
        final EditText trans = prom.findViewById(R.id.transporte);
        final Button canCant = prom.findViewById(R.id.canCantidad);
        final EditText totaltrans = prom.findViewById(R.id.totaltransporte);
        totaltrans.setEnabled(false);
        Button acepCantidad = prom.findViewById(R.id.acepCantidad);

        //edCant.setSelection(edCant.length());
        edCant.setText(category.get(eje).getCantidad());
        //Toast.makeText(Productos.this, edCant.getText()+"////////sdfsdfs", Toast.LENGTH_SHORT).show();
        edPrec.setText(category.get(eje).getPrecioUnitario());

        trans.setText(category.get(eje).getTransporte());

        double caltrans = Double.parseDouble(category.get(eje).getCantidad())*Double.parseDouble(category.get(eje).getTransporte());
        totaltrans.setText(String.valueOf(caltrans));

        trans.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String dtrans = "0";
                if (s.length() > 0){
                    dtrans = s.toString();
                }
                double caltransporte = Double.parseDouble(edCant.getText().toString()) * Double.parseDouble(dtrans);

                totaltrans.setText(String.valueOf(caltransporte));
            }
        });

        edCant.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String dcant = "0";
                if (s.length() > 0){
                    dcant = s.toString();
                }
                double caltransporte2 = Double.parseDouble(dcant) * Double.parseDouble(trans.getText().toString());

                totaltrans.setText(String.valueOf(caltransporte2));
            }
        });

        final String noma = category.get(eje).getNombre();
//        txtNom.setText(noma);
        canCant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sumar();
                aler.dismiss();
            }
        });
        acepCantidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edCant.length()==0)
                {
                    aler.dismiss();
                }
                else {
                    double cantidad = Double.parseDouble(edCant.getText().toString());
                    category.get(eje).setCantidad(cantidad+"");
                    //Double.parseDouble(category.get(eje).getPrecioTotal())
                    category.get(eje).setPrecioTotal(Double.toString(cantidad * Double.parseDouble(edPrec.getText().toString())));
                    category.get(eje).setTransporte(trans.getText().toString());
                    //adapter.notifyDataSetChanged();
                    lv.setAdapter(adapter);
                    sumar();
                    aler.dismiss();
                }
            }
        });

        aler.setView(prom);
        aler.show();
    }

    public void guardarTodo()
    {
        despacho.setDetalleDespacho(category);
        if (despacho.detallesDespacho.size()>0) {
            new GuardarDespacho().execute();
            Toast.makeText(Productos.this, "Pedido exitoso", Toast.LENGTH_SHORT).show();
            Intent i=new Intent(Productos.this, Select.class);
            i.putExtra("ID_EMPRESA",id_empresa);
            i.putExtra("ID_USUARIO",id_usuario);
            startActivity(i);
        }else{
//            Toast.makeText(Productos.this, "¡Debe agregar al menos un producto para realizar la transacción!", Toast.LENGTH_SHORT).show();
            Toast toast = Toast.makeText(Productos.this, "¡Debe agregar al menos un producto!", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    private class GuardarDespacho extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... data) {
            despacho.setDetalleDespacho(category);
            despacho.id_usuario=id_usuario;
            JSONObject datos = new JSONObject();
            try {
                datos.put("id_cliente",despacho.idCliente);
                datos.put("id_destino",despacho.idDestino);
                datos.put("id_cliente_razon",despacho.idClienteRazon);
                datos.put("fecha",despacho.fecha);
                datos.put("observacion",despacho.observaciones);
                datos.put("id_usuario",despacho.id_usuario);
                JSONArray detallesDespacho=new JSONArray();

                for (int i = 0; i < despacho.detallesDespacho.size(); i++) {
                    JSONObject jsonobj = new JSONObject();
                    jsonobj.put("id_producto", despacho.detallesDespacho.get(i).id_producto);
                    jsonobj.put("cantidad", despacho.detallesDespacho.get(i).cantidad);
                    jsonobj.put("precio_unitario", despacho.detallesDespacho.get(i).getPrecioUnitario());
                    jsonobj.put("total", despacho.detallesDespacho.get(i).precioTotal);
                    jsonobj.put("servicio_transporte", despacho.detallesDespacho.get(i).transporte);
                    jsonobj.put("latitud", getIntent().getDoubleExtra("latitud", 0));
                    jsonobj.put("longitud", getIntent().getDoubleExtra("longitud", 0));
                    detallesDespacho.put(jsonobj);
                }
                datos.put("detalles_despacho", detallesDespacho);
                Functions.postJSONdata("gtm-despacho/empresa/" + id_empresa, datos);


            } catch (JSONException ex) {
                //Logger.getLogger(EditPeymentConcept.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "";
        }
    }


}
