package xd.lotus.agil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import models.Producto;
import utils.Functions;

public class ProductoAdapter extends RecyclerView.Adapter<ProductoAdapter.MyViewHolder> {

    private List<Producto> productosList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView nombre;
        public ImageView imagen;

        public MyViewHolder(View view) {
            super(view);
            nombre = view.findViewById(R.id.nomView);
            imagen = view.findViewById(R.id.imgView);
        }
    }


    public ProductoAdapter(List<Producto> moviesList) {
        this.productosList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.reso, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Producto producto = productosList.get(position);
        holder.nombre.setText(producto.getNombre());

        LoadImageFromURL loadImage = new LoadImageFromURL(holder);
        loadImage.execute(Functions.SERVER_URL+"/"+producto.getImagen()/*.substring(1)*/);
    }

    public class LoadImageFromURL extends AsyncTask<String, Void, Bitmap> {

        MyViewHolder holder;
        public LoadImageFromURL(MyViewHolder holder){
            this.holder=holder;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            // TODO Auto-generated method stub

            try {
                URL url = new URL(params[0]);
                InputStream is = url.openConnection().getInputStream();
                Bitmap bitMap = BitmapFactory.decodeStream(is);
                return bitMap;

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(Bitmap result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            //iv.setImageBitmap(result);
            holder.imagen.setImageBitmap(result);
        }

    }

    @Override
    public int getItemCount() {
        return productosList.size();
    }
}